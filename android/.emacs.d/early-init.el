(setenv "PATH" (format "%s:%s" "/data/data/com.termux/files/usr/bin"
               (getenv "PATH")))
(setenv "LD_LIBRARY_PATH" (format "%s:%s"
                  "/data/data/com.termux/files/usr/lib"
                  (getenv "LD_LIBRARY_PATH")))
(setenv "INFOPATH" (format "%s:%s" "/data/data/com.termux/files/usr/share/info/"
                           (getenv "INFOPATH")))
(setenv "MANPATH" (format "%s:%s" "/data/data/com.termux/files/usr/share/man"
                          (getenv "MANPATH")))
(push "/data/data/com.termux/files/usr/bin" exec-path)
