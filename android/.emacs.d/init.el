;; -*- lexical-binding: t; -*-


(set-face-attribute 'default nil :height 500)
(setq touch-screen-display-keyboard t)

     (setq inhibit-startup-message t)
    ;;   (setq default-directory "~/Syncthing/Documents/Org-Notes/")
    (set-fringe-mode 10)

    (setq visible-bell t)

    (column-number-mode)
  ;; Enable line numbers for some modes
    (dolist (mode '(text-mode-hook
  	    prog-mode-hook
  	    conf-mode-hook))
(add-hook mode (lambda () (display-line-numbers-mode 1))))
    ;; Disable line numbers for some modes
    (dolist (mode '(org-mode-hook
  	    term-mode-hook
  	    shell-mode-hook
  	    treemacs-mode-hook
  	    eshell-mode-hook
  	    pdf-view-mode-hook))
(add-hook mode (lambda () (display-line-numbers-mode 0))))

(electric-pair-mode)

(setq gc-cons-threshold (* 50 1000 1000))

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections"
  	     (format "%.2f seconds"
  	     (float-time
  	      (time-subtract after-init-time before-init-time)))
  	     gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)
 ; Increase the amount of data Emacs reads from Process
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; Bootstrap for straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

  (straight-use-package 'use-package)
  (setq straight-use-package-by-default t)

(use-package use-ttf
  :config
  (setq use-ttf-default-ttf-font-name "Iosevka Nerd Font")
  (setq use-ttf-default-ttf-fonts '(
                                    "~/.emacs.d/fonts/ IosevkaNerdFont-Regular.ttf"
                                    "~/.emacs.d/fonts/FiraMonoNerdFontPropo-Regular.otf"
                                    )))

(use-package nerd-icons
  :custom
  (nerd-icons-font-family "Iosevka Nerd Font"))

(defun efs/org-font-setup ()
  (set-face-attribute 'org-document-title nil :font "Iosevka Nerd Font" :weight 'bold :height 1.5)

  (dolist (face '((org-level-1 . 1.75)
                  (org-level-2 . 1.5)
                  (org-level-3 . 1.25)
                  (org-level-4 . 1.1)
                  (org-level-5 . 1.0)
                  (org-level-6 . 1.0)
                  (org-level-7 . 1.0)
                  (org-level-8 . 1.0)))
  (set-face-attribute (car face) nil :font "FiraMono Nerd Font Propo" :weight 'regular :height (cdr face)))
  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (custom-theme-set-faces
   'user
   '(org-block ((t (:inherit fixed-pitch :foreground unspecified))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-checkbox ((t (:inherit fixed-pitch))))
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table ((t (:inherit fixed-pitch))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))))

(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1)
  )

(setq straight-fix-org t)
(use-package org
  :straight (org :type git :host github :repo "emacs-straight/org-mode")
  :hook
  (org-mode . efs/org-mode-setup)
  :bind
  ("C-c a" . org-agenda)
  :config
  ; Hide tags of entries of org agenda
  (setq org-agenda-hide-torg-auto-align-tags nil)
  (setq org-hide-emphasis-markers t)
  (setq org-ellipsis "...")
  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil)
  (setq org-pretty-entities t)
  (setq org-use-sub-superscripts "{}")
  (setq org-startup-indented t)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t))

(set-default-coding-systems 'utf-8)

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

;; Bindings with general.el
(use-package general
  :config
  ;; Creates a function for future key definitions
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")
  (general-create-definer dw/ctrl-c-keys
    :prefix "C-c")
  (rune/leader-keys
    "t" '(:ignore t :which-key "toggles")
    ;"tt" '(counsel-load-theme :which-key "choose theme")
    ))

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

  (rune/leader-keys
    "ts" '(hydra-text-scale/body :which-key "scale text"))

(use-package dashboard
  :straight t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-display-icons-p t)
  (setq dashboard-icon-type 'all-the-icons)
  (setq dashboard-projects-switch-function 'consult-projectile-switch-project)
  (setq dashboard-projects-backend 'projectile)
  (setq initial-buffer-choice (lambda () (get-buffer-create dashboard-buffer-name))))

(use-package doom-themes
    :straight t
    :config
    ;; Global settings (default)
    ;; (setq doom-themes-enable-bold t
    ;;       doom-themes-enable-italic t)
    (load-theme 'doom-horizon t))

;; ;; Add rainbow delimiters
;; ;
  				      ; Good for programming
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package emojify
  :hook (erc-mode . emojify-mode)
  :commands emojify-mode)

(setq display-time-format "%l:%M %p %b %y"
      display-time-default-load-average nil)

(use-package diminish)

;; First time this configuration is loaded on your machine
;; run the following command interactively:
;; M-x all-the-icons-install-fonts

(use-package all-the-icons
  :if (display-graphic-p))

(use-package doom-modeline
  :straight t
  :hook (after-init . doom-modeline-mode)
  :custom-face
  (mode-line ((t (:height 0.85))))
  (mode-line-inactive ((t (:height 0.85))))
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-except-project))

(use-package minions
  :hook (doom-modeline-mode . minions-mode))

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

(use-package hl-line
  :custom
  (global-hl-line-mode +1))

(use-package pulsar
  :config
  (setq pulsar-pulse t)
  (setq pulsar-delay 0.055)
  (setq pulsar-iterations 10)
  (setq pulsar-face 'pulsar-magenta)
  (setq pulsar-highlight-face 'pulsar-yellow)
  :bind
  (("C-c h p" . pulsar-pulse-line)
   ("C-c h h" . pulsar-highlight-line))
  :custom
  (pulsar-global-mode +1)
  (pulsar-pulse-functions
   '(
     backward-page
     bookmark-jump
     delete-other-windows
     delete-window
     forward-page
     goto-line
     handle-switch-frame
     logos-backward-page-dwim
     logos-forward-page-dwim
     handle-select-window
     move-to-window-line-top-bottom
     narrow-to-defun
     narrow-to-page
     narrow-to-region
     next-buffer
     next-multiframe-window
     org-backward-heading-same-level
     org-forward-heading-same-level
     org-next-visible-heading
     org-previous-visible-heading
     other-window
     outline-backward-same-level
     outline-forward-same-level
     outline-next-visible-heading
     outline-previous-visible-heading
     outline-up-heading
     previous-buffer
     recenter-top-bottom
     reposition-window
     scroll-down-command
     scroll-up-command
     tab-close
     tab-new
     tab-next
     widen
     windmove-down
     windmove-left
     windmove-right
     windmove-swap-states-down
     windmove-swap-states-left
     windmove-swap-states-right
     windmove-swap-states-up
     windmove-up
     )))

(setq dw/is-termux
      (string-suffix-p "Android" (string-trim (shell-command-to-string "uname -a"))))
(defun dw/replace-unicode-font-mapping (block-name old-font new-font)
    (let* ((block-idx (cl-position-if
                           (lambda (i) (string-equal (car i) block-name))
                           unicode-fonts-block-font-mapping))
           (block-fonts (cadr (nth block-idx unicode-fonts-block-font-mapping)))
           (updated-block (cl-substitute new-font old-font block-fonts :test 'string-equal)))
      (setf (cdr (nth block-idx unicode-fonts-block-font-mapping))
            `(,updated-block))))

  (use-package unicode-fonts
    :disabled
    :if (not dw/is-termux)
    :custom
    (unicode-fonts-skip-font-groups '(low-quality-glyphs))
    :config
    ;; Fix the font mappings to use the right emoji font
    (mapcar
      (lambda (block-name)
        (dw/replace-unicode-font-mapping block-name "Apple Color Emoji" "Noto Color Emoji"))
      '("Dingbats"
        "Emoticons"
        "Miscellaneous Symbols and Pictographs"
        "Transport and Map Symbols"))
    (unicode-fonts-setup))

(use-package indent-bars
  :straight (indent-bars :type git :host github :repo "jdtsmith/indent-bars")
  :custom
  ;; Add other languages as needed
  (indent-bars-treesit-scope '((python function_definition class_definition for_statement
                                       if_statement with_statement while_statement)))
  ;; wrap may not be needed if no-descend-list is enough
  ;;(indent-bars-treesit-wrap '((python argument_list parameters ; for python, as an example
  ;;				      list list_comprehension
  ;;				      dictionary dictionary_comprehension
  ;;				      parenthesized_expression subscript)))
  :hook ((python-base-mode yaml-mode) . indent-bars-mode)
  :config
  (setq
   indent-bars-color '(highlight :face-bg t :blend 0.3)
   indent-bars-pattern " . . . . ." ; play with the number of dots for your usual font size
   indent-bars-width-frac 0.25
   indent-bars-pad-frac 0.1)
  )

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

(use-package async
  :config
  (setq async-bytecomp-allowed-packages '(all))
  :custom
  (async-bytecomp-package-mode 1)
  )

(use-package savehist
  :init
  (savehist-mode)
  :config
  (setq history-length 100))

(defun dw/minibuffer-backward-kill (arg)
   "When minibuffer is completing a file name delete up to parent
 folder, otherwise delete a word"
   (interactive "p")
   (if minibuffer-completing-file-name
       ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
       (if (string-match-p "/." (minibuffer-contents))
           (zap-up-to-char (- arg) ?/)
         (delete-minibuffer-contents))
       (delete-word (- arg))))

(use-package vertico
  :bind (:map vertico-map
  	("C-j" . vertico-next)
  	("C-k" . vertico-previous)
  	("C-f" . vertico-exit)
       :map minibuffer-local-map
       ("M-h" . dw/minibuffer-backward-kill))
  :custom
  (vertico-cycle t)
  :custom-face
  (vertico-current ((t (:background "#3a3f5a"))))
  :init
  (vertico-mode))

(use-package cape
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  )

(defun corfu-enable-always-in-minibuffer ()
  "Enable Corfu in the minibuffer if Vertico/Mct are not active."
  (unless (or (bound-and-true-p mct--active)
              (bound-and-true-p vertico--input)
              (eq (current-local-map) read-passwd-map))
    ;; (setq-local corfu-auto nil) ;; Enable/disable auto completion
    (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                corfu-popupinfo-delay nil)
    (corfu-mode 1)))

(use-package nerd-icons-corfu
  :after corfu)

(use-package corfu-terminal
  :after corfu
  :straight t
  :init (when (not (display-graphic-p)) 
              (corfu-terminal-mode)))

(use-package corfu
  :bind
  (:map corfu-map
   ("TAB" . corfu-next)
   ([tab] . corfu-next)
   ("S-TAB" . corfu-previous)
   ([backtab] . corfu-previous))
  :init
  (corfu-history-mode)
  (corfu-popupinfo-mode)
  (global-corfu-mode)
  :custom
  (corfu-auto t)
  (corfu-auto-prefix 2)
  (corfu-quit-at-boundary 'separator)
  (corfu-preview-current 'insert)
  (corfu-preselect 'prompt)
  (corfu-cycle t)
  (corfu-separator ?\s)
  (tab-always-indent 'complete)
  :config
  (with-eval-after-load 'safehist
    (cl-pushnew 'corfu-history savehist-additional-variables))
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter)
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(defun dw/get-project-root ()
  (when (fboundp 'projectile-project-root)
    (projectile-project-root)))

(use-package consult
  :demand t
  :bind (("C-s" . consult-line)
       ("C-M-l" . consult-imenu)
       ("C-M-j" . persp-switch-to-buffer*)
       :map minibuffer-local-map
       ("C-r" . consult-history))
  :config
  ;; local modes added to prog-mode hooks
  (add-to-list 'consult-preview-allowed-hooks 'elide-head-mode)
  ;; enabled global modes
  (add-to-list 'consult-preview-allowed-hooks 'global-org-modern-mode)
  :custom
  (consult-customize consult--source-buffer :hidden t :default nil)
  (add-to-list 'consult-buffer-sources persp-consult-source)
  (consult-project-root-function #'dw/get-project-root)
  (completion-in-region-function #'consult-completion-in-region))

(use-package consult-dir
  :after consult
  :straight t
  :bind (("C-x C-d" . consult-dir)
       :map vertico-map
       ("C-x C-d" . consult-dir)
       ("C-x C-j" . consult-dir-jump-file))
  :custom
  (consult-dir-project-list-function nil))

(with-eval-after-load 'eshell-mode
(defun eshell/z (&optional regexp)
  "Navigate to a previously visited directory in eshell."
  (let ((eshell-dirs (delete-dups (mapcar 'abbreviate-file-name
                                          (ring-elements eshell-last-dir-ring)))))
    (cond
     ((and (not regexp) (featurep 'consult-dir))
      (let* ((consult-dir--source-eshell `(:name "Eshell"
                                                 :narrow ?e
                                                 :category file
                                                 :face consult-file
                                                 :items ,eshell-dirs))
             (consult-dir-sources (cons consult-dir--source-eshell consult-dir-sources)))
        (eshell/cd (substring-no-properties (consult-dir--pick "Switch directory: ")))))
     (t (eshell/cd (if regexp (eshell-find-previous-directory regexp)
                     (completing-read "cd: " eshell-dirs))))))))

(use-package marginalia
  :after vertico
  :bind
  (:map minibuffer-local-map
       ("M-A" . marginalia-cycle))
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package embark
  :bind (("C-S-a" . embark-act)
         ("C-;" . embark-dwim)
  	     :map minibuffer-local-map
  	     ("C-d" . embark-act))
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  (setq embark-action-indicator
        (lambda (map)
          (which-key--show-keymap "Embark" map nil nil 'no-paging)
          #'which-key--hide-popup-ignore-command)
        embark-become-indicator embark-action-indicator))

(use-package embark-consult
  :after embark
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :straight t
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key)
  )

(use-package perspective
  :demand t
  :bind (("C-x b" . consult-buffer)
         ("C-M-k" . persp-switch)
         ("C-M-n" . persp-next)
         ("C-x k" . persp-kill-buffer*))
  :custom
  (persp-initial-frame-name "Main")
  (persp-mode-prefix-key (kbd "C-c M-p"))
  (persp-state-default-file "~/.emacs.d/persp-state")
  :config
  ;; Running `persp-mode' multiple times resets the perspective list...
  (unless (equal persp-mode t)
    (persp-mode)))

(defun efs/org-font-setup ()
  (set-face-attribute 'org-document-title nil :font "FiraMono Nerd Font Propo" :weight 'bold :height 1.5)

  (dolist (face '((org-level-1 . 1.75)
                  (org-level-2 . 1.5)
                  (org-level-3 . 1.25)
                  (org-level-4 . 1.1)
                  (org-level-5 . 1.0)
                  (org-level-6 . 1.0)
                  (org-level-7 . 1.0)
                  (org-level-8 . 1.0)))
  (set-face-attribute (car face) nil :font "FiraMono Nerd Font Propo" :weight 'regular :height (cdr face)))
  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (custom-theme-set-faces
   'user
   '(org-block ((t (:inherit fixed-pitch :foreground unspecified))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-checkbox ((t (:inherit fixed-pitch))))
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table ((t (:inherit fixed-pitch))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))))

(use-package org-super-agenda
  :after org
  :custom
  (org-super-agenda-mode +1)
  :config
  (setq org-super-agenda-groups
        '(
          (:name "Routine"
                 :habit t
                 :order 1)
          (:name "General Todo"
                 :tag "general"
                 :order 3)
          (:name "University Related Todo"
                 :tag "uni"
                 :order 4)
          (:name "Learning"
                 :tag "learning"
                 :order 4)
          (:name "Self Hosting"
                 :tag "self_hosting"
                 :order 5)
          (:name "System Crafting"
                 :tag "system_crafting"
                 :order 5)
          (:name "Events Today"
                 :tag "event"
                 :order 2)
          (:name "Reading"
                 :and (:todo "TO-READ" :tag ("book" "article" "manga")))
          (:discard (:anything t))
          (:name "Going to Watch"
                 :todo "TO-WATCH")
          (:name "Important"
                 :priority "A"
                 :order 1)
          ))
  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
   '(("d" "Dashboard"
     ((agenda "" ((org-agenda-span 'day)
                  (org-super-agenda-groups
                   '(
                     ; Habit only groups items with PROPERTY STYLE:habit
                     (:name "Habit"
                            :habit t
                            :scheduled today)
                     (:name "Today"
                            :todo "TODAY"
                            :date today
                            :scheduled today)
                     (:name "Due Today"
                            :time-grid t
                            :deadline today)
                     (:name "Events Today"
                            :time-grid t
                            :tag "event"
                            :scheduled today)
                     (:name "Overdue"
                            :deadline past)
                     (:name "Due Soon"
                            :deadline future)
                     (:name "Events Coming Soon"
                            :tag "event"
                            :scheduled future)
                     (:name "Done today"
                            :and (:regexp "State \"DONE\""
                                          :log t))
                     (:name "Clocked today"
                            :log t)
                     ))
                  ))
      (alltodo "" ((org-agenda-overriding-header "")))
     (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
      (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))

    ("W" "Work Tasks" tags-todo "+work-email")

    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))
    ("w" "Workflow Status"
      ((todo "WAIT"
            ((org-agenda-overriding-header "Waiting on External")
             (org-agenda-files org-agenda-files)))
      (todo "REVIEW"
            ((org-agenda-overriding-header "In Review")
             (org-agenda-files org-agenda-files)))
      (todo "PLAN"
            ((org-agenda-overriding-header "In Planning")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "BACKLOG"
            ((org-agenda-overriding-header "Project Backlog")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "READY"
            ((org-agenda-overriding-header "Ready for Work")
             (org-agenda-files org-agenda-files)))
      (todo "ACTIVE"
            ((org-agenda-overriding-header "Active Projects")
             (org-agenda-files org-agenda-files)))
      (todo "COMPLETED"
            ((org-agenda-overriding-header "Completed Projects")
             (org-agenda-files org-agenda-files)))
      (todo "CANC"
            ((org-agenda-overriding-header "Cancelled Projects")
             (org-agenda-files org-agenda-files)))))))
                   )))

(use-package org-appear
  :after org
  :hook
  (org-mode . org-appear-mode))

; org-bullets is no longer maintained, using its successor
(use-package org-superstar
  :after org
  :hook (org-mode . org-superstar-mode)
  :config
  ;(org-superstar-configure-like-org-bullets)
  (setq org-superstar-remove-leading-stars t)
  (setq org-superstar-headline-bullets-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :defer t
  :hook (org-mode . efs/org-mode-visual-fill)
  :config
  (add-hook 'org-agenda-finalize-hook #'visual-fill-column-mode))

(use-package org-modern
  :straight t
  :custom
  (org-modern-list 
   '((?- . "-")
     (?* . "•")
     (?+ . "‣")))
  (org-modern-block-name '("" . ""))
  :custom
  (org-modern-table nil)
  :config
  (add-hook 'org-agenda-finalize-hook #'org-modern-agenda)
  :hook
  (org-mode . org-modern-mode))


(use-package org-modern-indent
  :demand t
  :straight (org-modern-indent :type git :host github :repo "jdtsmith/org-modern-indent")
  :config
  (add-hook 'org-mode-hook #'org-modern-indent-mode 90))

(with-eval-after-load 'org
  (setq org-babel-lisp-eval-fn 'sly-eval)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (java . t)
     (lisp . t)))
  (push '("conf-unix" . conf-unix) org-src-lang-modes))

(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

(with-eval-after-load 'org
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python")))
(add-to-list 'org-structure-template-alist '("li" . "src lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))

(use-package org-roam
  :straight t
  :after org
  :custom
  (org-roam-directory (file-truename "~/Syncthing/Documents/Org-Roam-Notes/"))
  (org-roam-dailies-directory "Journal/")
  (setq org-roam-complete-everywhere t)
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  (org-roam-capture-templates
    '(("d" "default" plain
       "%?"
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
       :unnarrowed t)
      ("b" "book notes" plain
       (file "~/Syncthing/Documents/Org-Roam-Notes/Templates/BookNote.org")
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
      ("p" "project file" plain
       "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add Initial tasks \n\n * Dates \n\n"
       ; Add a tag of project to make it easy to query and filter org-roam notes
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;;Dailies
         ("C-c n j" . org-roam-dailies-capture-today)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  (require 'org-roam-dailies)
  (require 'org-roam-protocol))

(use-package org-roam-ui
  :straight
    (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
    :after org-roam
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(use-package org-download
  :straight (org-download :type git :host github :repo "abo-abo/org-download")
  :config
  (add-hook 'dired-mode-hook 'org-download-enable)
  (rune/leader-keys
    "pp"  '(org-download-clipboard :which-key "Paste Image from Clipboard")))

(use-package org-cliplink
  :after org
  :bind
  ("C-x p i" . org-cliplink))

(defun org-image-link (protocol link _description)
  "Interpret LINK as base64-encoded image data."
  (cl-assert (string-match "\\`img" protocol) nil
             "Expected protocol type starting with img")
  (let ((buf (url-retrieve-synchronously (concat (substring protocol 3) ":" link))))
    (cl-assert buf nil
               "Download of image \"%s\" failed." link)
    (with-current-buffer buf
      (goto-char (point-min))
      (re-search-forward "\r?\n\r?\n")
      (buffer-substring-no-properties (point) (point-max)))))

(use-package org-yt
    :straight (org-yt :type git :host github :repo "TobiasZawada/org-yt")
    :config
    (org-link-set-parameters
     "imghttp"
     :image-data-fun #'org-image-link)

    (org-link-set-parameters
     "imghttps"
     :image-data-fun #'org-image-link))

(use-package org-transclusion
  :after org
  :config
  (define-key global-map (kbd "<f12>") #'org-transclusion-add)
  (rune/leader-keys
      "nt" '(org-transclusion-mode :which-key "Turn on Org-Transclusion")))

(use-package toc-org
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

(with-eval-after-load 'markdown-mode
  (define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point))

(use-package rainbow-mode
  :defer t
  :hook (org-mode
         emacs-lisp-mode
         web-mode
         typescript-mode
         js2-mode))

(use-package expand-region
  :bind (("M-[" . er/expand-region)
         ("C-(" . er/mark-outside-pairs)))

(setq-default tab-width 2)

(setq-default indent-tabs-mode nil)

(defun dw/switch-project-action ()
  "Switch to a workspace with the project name."
  (persp-switch (projectile-project-name)))

  (use-package projectile
    :diminish projectile-mode
    :config (projectile-mode)
    :demand t
    :bind ("C-M-p" . projectile-find-file)
    :bind-keymap
    ("C-c p" . projectile-command-map)
    :init
    (when (file-directory-p "~/Projects/Code")
      (setq projectile-project-search-path '("~/Projects/Code"
                                             "~/Syncthing/Sync-Essentials/books"
                                             "~/Syncthing/Documents/Org-Notes/"
                                             "~/Syncthing/Documents/Org-Roam-Notes/"))
    (setq projectile-switch-project-action #'dw/switch-project-action)))

(use-package consult-projectile
  :after projectile)
(rune/leader-keys
  "pF" 'consult-ripgrep)

(use-package magit
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package forge
  :after magit)

(use-package go-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode)))

(use-package gorepl-mode
  :after go-mode
  :hook
  (go-mode . gorepl-mode))

(use-package go-playground
  :after go-mode)

(use-package geiser-guile)

(use-package exec-path-from-shell)

(dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "NIX_PROFILES"))
    (add-to-list 'exec-path-from-shell-variables var))

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
(when (daemonp)
  (exec-path-from-shell-initialize))

(defun efs/configure-eshell ()
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
  ;(evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (evil-normalize-keymaps)
  (setq eshell-history-size 10000
    eshell-buffer-maximum-lines 10000
    eshell-hist-ignoredups t
    eshell-scroll-to-bottom-on-input t))

(defun corfu-send-shell (&rest _)
  "Send completion candidate when inside comint/eshell."
  (cond
  ((and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
    (eshell-send-input))
  ((and (derived-mode-p 'comint-mode)  (fboundp 'comint-send-input))
    (comint-send-input))))

(use-package eshell-prompt-extras
  :after eshell)

(with-eval-after-load 'esh-opt
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-highlight-prompt nil
        eshell-prompt-function 'epe-theme-lambda))

(use-package eshell
  :hook
  (eshell-first-time-mode . efs/configure-eshell)
  :config
  (advice-add #'corfu-insert :after #'corfu-send-shell)

  (add-hook 'eshell-mode-hook
        (lambda ()
          (setq-local
           corfu-quit-at-boundary t
           corfu-quit-no-match t
           corfu-auto nil)
          (corfu-mode)))
  (setq eshell-aliases-file "~/.emacs.d/.eshell.aliases")
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim" "gpg"))))

(use-package eshell-up
  :after eshell)

(straight-use-package
 '(aweshell :type git
      :host github
      :repo "manateelazycat/aweshell"))

(use-package aweshell
  :straight t
  :after eshell
  :bind
  ("C-c e t" . aweshell-new)
  ("C-c e n" . aweshell-next)
  ("C-c e p" . aweshell-prev)
  ("C-c e d" . aweshell-dedicated-toggle))

(setq gc-cons-threshold (* 2 1000 1000))
