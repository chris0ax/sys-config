let
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/nixos-24.05";
  pkgs = import nixpkgs { config = { }; overlays = [ ]; };
in

pkgs.mkShellNoCC {
  packages = with pkgs; [
    python312Full
    ruff-lsp
    ruff
    # Add rope support for pylsp?
    python312Packages.python-lsp-server
  ];
}
