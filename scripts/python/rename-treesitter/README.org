#+title: Python: Symlink treesitter language grammars

* Sources
- [[https://libreddit.projectsegfau.lt/r/emacs/comments/zbpa42/how_to_use_emacs_29_treesitter/][How to use Emacs 29 Tree-sitter? - r/emacs]]
- [[https://www.masteringemacs.org/article/how-to-get-started-tree-sitter][How to Get Started with Tree-Sitter - Mastering Emacs]] 

* Background + Algorithm
This small script was created because the =tree-sitter-langs= emacs package downloads and compiles the language grammars for use with =*-ts-mode= in =tree-sitter= for Emacs 29+ automatically but they seem to need to be renamed properly to be able to be detected.


For =tree-sitter= to enable a specific =ts-mode= for a language, it must find the language grammar in either the system shared libraries, =~/.emacs.d/tree-sitter= or the paths mentioned in =treesit-extra-load-path=. Not only this, but they must be of the format =libtree-sitter-<name>.so= for it to detect and work properly.

To remedy this, this script looks at the file directory listing for =~/.emacs.d/straight/build/tree-sitter-langs/bin/=, stores the file names of the compiled shared libraries in a list, then creates a symlink to =~/.emacs.d/tree-sitter= in a loop iterating through each file, where the symlink is named in the format =libtree-sitter-${current_file_name}.so=

#+begin_src python :tangle symlink_treesitter.py
  import os
  tree_sitter_langs_path = f"{os.environ['HOME']}/.emacs.d/straight/build/tree-sitter-langs/bin"
  working_dir = os.listdir(path=tree_sitter_langs_path)
  target_dir = f"{os.environ['HOME']}/.emacs.d/tree-sitter"
  if not os.path.exists(target_dir):
      print(f"{target_dir} does not exist! Creating directory..")
      os.mkdir(target_dir)
  for file in working_dir:
      if file == "BUNDLE-VERSION":
          continue
      print(f"Symlinking {tree_sitter_langs_path}/{file} to {target_dir}/libtree-sitter-{file} ...")
      os.symlink(f"{tree_sitter_langs_path}/{file}", f"{target_dir}/libtree-sitter-{file}")
      
#+end_src

#+RESULTS:
