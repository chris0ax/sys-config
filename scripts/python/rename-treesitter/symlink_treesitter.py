import os

tree_sitter_langs_path = (
    f"{os.environ['HOME']}/.emacs.d/straight/build/tree-sitter-langs/bin"
)
working_dir = os.listdir(path=tree_sitter_langs_path)
target_dir = f"{os.environ['HOME']}/.emacs.d/tree-sitter"
if not os.path.exists(target_dir):
    print(f"{target_dir} does not exist! Creating directory..")
    os.mkdir(target_dir)
for file in working_dir:
    if file == "BUNDLE-VERSION":
        continue
    print(
        f"Symlinking {tree_sitter_langs_path}/{file} to {target_dir}/libtree-sitter-{file} ..."
    )
    os.symlink(
        f"{tree_sitter_langs_path}/{file}", f"{target_dir}/libtree-sitter-{file}"
    )
