;; -*- lexical-binding: t; -*-


  (guix-emacs-autoload-packages)

  (defvar runemacs/default-font-size 150)
  (setq inhibit-startup-message t)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (setq default-directory "~/Syncthing/Documents/Org-Notes/")
  (tooltip-mode -1)
  (set-fringe-mode 10)

  (menu-bar-mode -1)

  (setq visible-bell t)

  (column-number-mode)
;; Enable line numbers for some modes
  (dolist (mode '(text-mode-hook
                  prog-mode-hook
                  conf-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 1))))
  ;; Disable line numbers for some modes
  (dolist (mode '(org-mode-hook
                  term-mode-hook
                  shell-mode-hook
                  treemacs-mode-hook
                  eshell-mode-hook
                  pdf-view-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 0))))

  ;; (global-display-line-numbers-mode t)

(defun efs/set-font-faces()

  (set-face-attribute 'default nil :family "Iosevka Nerd Font" :height runemacs/default-font-size)
  
  ;Set fixed pitch face
  (set-face-attribute 'fixed-pitch nil :family "Iosevka Nerd Font" :height 150)

  ;; Set variable pitchface
  ;;(set-face-attribute 'variable-pitch nil :family "Iosevka Aile" :height 200 :weight 'regular)
  ;; Check out IBM Plex Sans
  ;; Fantasque Sans
  ;; https://input.djr.com
  (set-face-attribute 'variable-pitch nil :family "FiraMono Nerd Font Propo" :height 150 :weight 'normal))

(if (daemonp)
    (add-hook 'after-make-frame-functions
	  (lambda (frame)
	    (setq doom-modeline-icon t)
	    (with-selected-frame frame
	      (efs/set-font-faces))))
  (efs/set-font-faces))

(use-package emacs
  :config
  (setq mouse-drag-and-drop-region-cross-program t)
  (setq mouse-1-click-follows-link nil)
  (setq ffap-require-prefix t)
  (ffap-bindings)
  (require 'kmacro)
  (defalias 'kmacro-insert-macro 'insert-kbd-macro)
  (define-key kmacro-keymap (kbd "I") #'kmacro-insert-macro)
  :bind
  ("M-S-z" . zap-up-to-char)
  ("M-s M-s" . isearch-forward)
  (:map isearch-mode-map
        ("M-s e" . consult-isearch-history))
  ([remap list-buffers] . ibuffer)
  ("C-x t [" . tab-bar-history-back)
  ("C-x t ]" . tab-bar-history-forward)
  :custom
  ;; TAB cycle if there are only few candidates
  ;; (completion-cycle-threshold 3)
  
  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (winner-mode t)
  (tab-bar-history-mode t)
  (repeat-mode t)
  (isearch-allow-motion t)
  (tab-always-indent 'complete)

  (pixel-scroll-precision-mode t)
  ;; Emacs 30 and newer: Disable Ispell completion function. As an alternative,
  ;; try `cape-dict'.
  (text-mode-ispell-word-completion nil)

  ;; Emacs 28 and newer: Hide commands in M-x which do not apply to the current
  ;; mode.  Corfu commands are hidden, since they are not used via M-x. This
  ;; setting is useful beyond Corfu.
  (read-extended-command-predicate #'command-completion-default-include-p))

(setq gc-cons-threshold (* 50 1000 1000))

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections"
	       (format "%.2f seconds"
	       (float-time
		(time-subtract after-init-time before-init-time)))
	       gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)
 ; Increase the amount of data Emacs reads from Process
(setq read-process-output-max (* 10 1024 1024)) ;; 10mb

;; Change the user-emacs-directory to keep unwanted things out of ~/.emacs.d
(setq user-emacs-directory (expand-file-name "~/.cache/emacs/")
      url-history-file (expand-file-name "url/history" user-emacs-directory))

;; Use no-littering to automatically set common paths to the new user-emacs-directory
(use-package no-littering)

;; Keep customization settings in a temporary file (thanks Ambrevar!)
(setq custom-file
      (if (boundp 'server-socket-dir)
          (expand-file-name "custom.el" server-socket-dir)
        (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))
(load custom-file t)

(set-default-coding-systems 'utf-8)

(use-package vundo)

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(with-eval-after-load 'meow
(defun meow-setup ()
  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   ;; SPC j/k will run the original command in MOTION state.
   '("j" . "H-j")
   '("k" . "H-k")
   ;; Use SPC (0-9) for digit arguments.
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . meow-beginning-of-thing)
   '("]" . meow-end-of-thing)
   '("a" . meow-append)
   '("A" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("d" . meow-delete)
   '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . meow-next-symbol)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '("I" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . meow-join)
   '("n" . meow-search)
   '("o" . meow-block)
   '("O" . meow-to-block)
   '("p" . meow-yank)
   '("q" . meow-quit)
   '("Q" . meow-goto-line)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-visit)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . meow-goto-line)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '("<escape>" . ignore))))

(use-package meow
  :bind
  ("C-G" . meow-insert-exit)
  :custom
  (meow-global-mode 1)
  :hook
  (meow-mode . meow-setup)
  (ielm-mode . (lambda ()
                 (meow-mode -1)
                 )))

(use-package meow-tree-sitter)

(use-package hl-line
  :custom
  (global-hl-line-mode +1))

(use-package pulsar
  :config
  (setq pulsar-pulse t)
  (setq pulsar-delay 0.055)
  (setq pulsar-iterations 10)
  (setq pulsar-face 'pulsar-magenta)
  (setq pulsar-highlight-face 'pulsar-yellow)
  :bind
  (("C-c h p" . pulsar-pulse-line)
   ("C-c h h" . pulsar-highlight-line))
  :custom
  (pulsar-global-mode +1)
  (pulsar-pulse-functions
   '(
     evil-window-up
     evil-window-down
     evil-window-left
     evil-window-right
     backward-page
     bookmark-jump
     delete-other-windows
     delete-window
     evil-goto-first-line
     evil-goto-line
     evil-scroll-down
     evil-scroll-line-to-bottom
     evil-scroll-line-to-center
     evil-scroll-line-to-top
     evil-scroll-page-down
     evil-scroll-page-up
     evil-scroll-up
     forward-page
     goto-line
     handle-switch-frame
     logos-backward-page-dwim
     logos-forward-page-dwim
     handle-select-window
     move-to-window-line-top-bottom
     narrow-to-defun
     narrow-to-page
     narrow-to-region
     next-buffer
     next-multiframe-window
     org-backward-heading-same-level
     org-forward-heading-same-level
     org-next-visible-heading
     org-previous-visible-heading
     other-window
     outline-backward-same-level
     outline-forward-same-level
     outline-next-visible-heading
     outline-previous-visible-heading
     outline-up-heading
     previous-buffer
     recenter-top-bottom
     reposition-window
     scroll-down-command
     scroll-up-command
     tab-close
     tab-new
     tab-next
     widen
     windmove-down
     windmove-left
     windmove-right
     windmove-swap-states-down
     windmove-swap-states-left
     windmove-swap-states-right
     windmove-swap-states-up
     windmove-up
     )))

(use-package indent-bars
  :custom
  ;; Add other languages as needed
  (indent-bars-treesit-scope '((python function_definition class_definition for_statement
                                       if_statement with_statement while_statement)))
  ;; wrap may not be needed if no-descend-list is enough
  ;;(indent-bars-treesit-wrap '((python argument_list parameters ; for python, as an example
  ;;				      list list_comprehension
  ;;				      dictionary dictionary_comprehension
  ;;				      parenthesized_expression subscript)))
  :hook ((python-base-mode yaml-mode) . indent-bars-mode)
  :config
  (setq
   indent-bars-color '(highlight :face-bg t :blend 0.3)
   indent-bars-pattern " . . . . ." ; play with the number of dots for your usual font size
   indent-bars-width-frac 0.25
   indent-bars-pad-frac 0.1)
  )

(use-package emojify
  :hook
  (erc-mode . emojify-mode)
  (mastodon-mode . emojify-mode)
  (ement . emojify-mode)
  :commands emojify-mode)

(use-package dashboard
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-display-icons-p t)
  (setq dashboard-icon-type 'all-the-icons)
  (setq dashboard-projects-switch-function 'consult-projectile-switch-project)
  (setq dashboard-projects-backend 'projectile)
  (setq initial-buffer-choice (lambda () (get-buffer-create dashboard-buffer-name))))

(use-package doom-themes)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(setq display-time-format "%l:%M %p %b %y"
        display-time-default-load-average nil)

;; First time this configuration is loaded on your machine
  ;; run the following command interactively:
  ;; M-x all-the-icons-install-fonts

(use-package all-the-icons
    :if (display-graphic-p))

(use-package minions
    :hook (doom-modeline-mode . minions-mode))

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :custom-face
  (mode-line ((t (:height 0.85))))
  (mode-line-inactive ((t (:height 0.85))))
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-except-project))

(use-package diminish)

(customize-set-variable 'display-buffer-base-action
  '((display-buffer-reuse-window display-buffer-same-window)
    (reusable-frames . t)))

(customize-set-variable 'even-window-sizes nil)     ; avoid resizing

(use-package alert
  :commands alert
  :config
  (setq alert-default-style 'notifications))

(use-package super-save
  :config
  (super-save-mode +1)
  (setq super-save-remote-files nil)
  (setq auto-save-default nil))
  (setq super-save-auto-save-when-idle t)
  (setq super-save-exclude '("Emacs-Conf.org"))

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

(setq display-time-world-list
  '(("Etc/UTC" "UTC")
    ("America/Los_Angeles" "Seattle")
    ("America/New_York" "New York")
    ("Europe/Athens" "Athens")
    ("Pacific/Auckland" "Auckland")
    ("Asia/Shanghai" "Shanghai")
    ("Asia/Kolkata" "Hyderabad")))
(setq display-time-world-time-format "%a, %d %b %I:%M %p %Z")

(use-package savehist
  :init
  (savehist-mode)
  :config
  (setq history-length 100))

(defun dw/minibuffer-backward-kill (arg)
   "When minibuffer is completing a file name delete up to parent
 folder, otherwise delete a word"
   (interactive "p")
   (if minibuffer-completing-file-name
       ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
       (if (string-match-p "/." (minibuffer-contents))
           (zap-up-to-char (- arg) ?/)
         (delete-minibuffer-contents))
       (delete-word (- arg))))

(use-package vertico
  :bind (:map vertico-map
	  ("C-j" . vertico-next)
	  ("C-k" . vertico-previous)
	  ("C-f" . vertico-exit)
	 :map minibuffer-local-map
	 ("M-h" . dw/minibuffer-backward-kill))
  :custom
  (vertico-cycle t)
  :custom-face
  (vertico-current ((t (:background "#3a3f5a"))))
  :init
  (vertico-mode))

;; Configure Tempel
(use-package tempel
  ;; Require trigger prefix before template name when completing.
  :after guix
  :custom
  (tempel-trigger-prefix "-")

  :config
  (setq tempel-path (list (concat guix-home-profile "/profile/share/emacs/site-lisp/tempel-collection-0.1-1.4a1d717/templates/*/*.eld")))
  :bind (("M-+" . tempel-complete) ;; Alternative tempel-expand
         ("M-*" . tempel-insert))
  :init
  ;; Setup completion at point
  (defun tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions)))

  (add-hook 'conf-mode-hook 'tempel-setup-capf)
  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf)

  ;; Optionally make the Tempel templates available to Abbrev,
  ;; either locally or globally. `expand-abbrev' is bound to C-x '.
  ;; (add-hook 'prog-mode-hook #'tempel-abbrev-mode)
  ;; (global-tempel-abbrev-mode)
)

;; Optional: Add tempel-collection.
;; The package is young and doesn't have comprehensive coverage.
(use-package tempel-collection
  :ensure t
  :after tempel)

(defun chr/cape-capf-setup-elisp()
  "Replace the default `elisp-completion-at-point'
completion-at-point-function. Doing it this way will prevent
disrupting the addition of other capfs (e.g. merely setting the
variable entirely, or adding to list).

Additionally, add `cape-file' as early as possible to the list."
  (add-to-list 'completion-at-point-functions #'elisp-completion-at-point)
  (add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  ;; I prefer this being early/first in the list
  (add-to-list 'completion-at-point-functions #'cape-file))

(defun chr/cape-capf-setup-org ()
  (let (result)
  (dolist (element (list
		                  (cape-capf-super #'cape-dict #'cape-dabbrev))
		                 result)
    (add-to-list 'completion-at-point-functions element)))

(add-to-list 'completion-at-point-functions #'cape-elisp-block)
(add-to-list 'completion-at-point-functions #'cape-keyword))

(defun chr/cape-capf-setup-eshell ()
  (add-to-list 'completion-at-point-functions #'cape-history)
  (add-to-list 'completion-at-point-functions #'cape-file))

(defun chr/cape-capf-setup-git-commit ()
  (let ((result))
    (dolist (element '(cape-dabbrev cape-keyword) result)
      (add-to-list 'completion-at-point-functions element))))

(defun chr/cape-capf-setup-latex ()
  (add-to-list 'completion-at-point-functions #'cape-tex)
  (let (result)
    (dolist (element (list
		                  (cape-capf-super #'cape-dict #'cape-dabbrev))
		                 result)
      (add-to-list 'completion-at-point-functions element))))

(use-package cape
  :hook
  (emacs-lisp-mode . chr/cape-capf-setup-elisp)
  (org-mode . chr/cape-capf-setup-org)
  (eshell-mode . chr/cape-capf-setup-eshell)
  (git-commit-mode . chr/cape-capf-setup-git-commit)
  (LaTeX-mode . chr/cape-capf-setup-latex))

(defun corfu-enable-always-in-minibuffer ()
  "Enable Corfu in the minibuffer if Vertico/Mct are not active."
  (unless (or (bound-and-true-p mct--active)
              (bound-and-true-p vertico--input)
              (eq (current-local-map) read-passwd-map))
    ;; (setq-local corfu-auto nil) ;; Enable/disable auto completion
    (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                corfu-popupinfo-delay nil)
    (corfu-mode 1)))

(use-package nerd-icons-corfu
  :after corfu)

(use-package corfu-candidate-overlay
 :after corfu
 :config
 ;; enable corfu-candidate-overlay mode globally
 ;; this relies on having corfu-auto set to nil
 (corfu-candidate-overlay-mode +1)
 ;; bind Ctrl + TAB to trigger the completion popup of corfu
 (global-set-key (kbd "C-<tab>") 'completion-at-point)
 ;; bind Ctrl + Shift + Tab to trigger completion of the first candidate
 ;; (keybing <iso-lefttab> may not work for your keyboard model)
 (global-set-key (kbd "C-S-<tab>") 'corfu-candidate-overlay-complete-at-point))

(use-package corfu
  :bind
  (:map corfu-map
   ("TAB" . corfu-next)
   ([tab] . corfu-next)
   ("S-TAB" . corfu-previous)
   ([backtab] . corfu-previous)
   ("S-<return>" . corfu-insert)
   ("RET"        . corfu-insert)
   ("M-SPC"      . corfu-insert-separator)
   )
  :init
  (corfu-history-mode)
  (corfu-popupinfo-mode)
  (global-corfu-mode)
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-auto-prefix 3)
  (corfu-auto-delay 1)
  (corfu-popupinfo-delay '(0.5 . 0.2))
  (corfu-quit-at-boundary 'separator)
  (corfu-preview-current 'insert)
  (corfu-preselect 'prompt)
  (corfu-separator ?\s)
  (corfu-on-exact-match nil)
  (tab-always-indent 'complete)
  :config
  (with-eval-after-load 'safehist
    (cl-pushnew 'corfu-history savehist-additional-variables))
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter)
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(defun dw/get-project-root ()
  (when (fboundp 'projectile-project-root)
    (projectile-project-root)))

(use-package consult
  :demand t
  :bind (("C-s" . consult-line)
         ("C-x M-s" . consult-line-multi)
         ("C-c c g" . consult-grep)
         ("C-c c f" . consult-find)
         ("C-c c i" . consult-info)
         ("C-c c h" . consult-man)
         ("C-c c d" . consult-flymake)
         ("C-c c m" . consult-mark)
         ("C-c c b" . consult-bookmark)
         ("C-c c o" . consult-org-heading)
         ("C-x M-:" . consult-complex-command)
	       ("C-M-l" . consult-imenu)
	       ("C-M-j" . consult-buffer)
	       :map minibuffer-local-map
	       ("C-r" . consult-history))
  :config
  ;; local modes added to prog-mode hooks
  (add-to-list 'consult-preview-allowed-hooks 'elide-head-mode)
  ;; enabled global modes
  (add-to-list 'consult-preview-allowed-hooks 'global-org-modern-mode)
  :custom
  (consult-customize consult--source-buffer :hidden t :default nil)
  (add-to-list 'consult-buffer-sources persp-consult-source)
  (consult-project-root-function #'dw/get-project-root)
  (completion-in-region-function #'consult-completion-in-region))

(use-package wgrep)

(use-package consult-dir
  :after consult
  :bind (("C-x C-d" . consult-dir)
	 :map vertico-map
	 ("C-x C-d" . consult-dir)
	 ("C-x C-j" . consult-dir-jump-file))
  :custom
  (consult-dir-project-list-function nil))

(with-eval-after-load 'eshell-mode
(defun eshell/z (&optional regexp)
  "Navigate to a previously visited directory in eshell."
  (let ((eshell-dirs (delete-dups (mapcar 'abbreviate-file-name
                                          (ring-elements eshell-last-dir-ring)))))
    (cond
     ((and (not regexp) (featurep 'consult-dir))
      (let* ((consult-dir--source-eshell `(:name "Eshell"
                                                 :narrow ?e
                                                 :category file
                                                 :face consult-file
                                                 :items ,eshell-dirs))
             (consult-dir-sources (cons consult-dir--source-eshell consult-dir-sources)))
        (eshell/cd (substring-no-properties (consult-dir--pick "Switch directory: ")))))
     (t (eshell/cd (if regexp (eshell-find-previous-directory regexp)
                     (completing-read "cd: " eshell-dirs))))))))

(use-package marginalia
  :after vertico
  :bind
  (:map minibuffer-local-map
       ("M-A" . marginalia-cycle))
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package nerd-icons-completion
    :after marginalia
    :config
    (nerd-icons-completion-mode)
    (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package embark
  :bind (("C-S-a" . embark-act)
         ("C-;" . embark-dwim)
	       :map minibuffer-local-map
	       ("C-d" . embark-act))
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  (setq embark-action-indicator
        (lambda (map)
          (which-key--show-keymap "Embark" map nil nil 'no-paging)
          #'which-key--hide-popup-ignore-command)
        embark-become-indicator embark-action-indicator))

(use-package embark-consult
  :after embark
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key)
  )

(use-package avy
  :commands (avy-goto-char avy-goto-word-0 avy-goto-line))

(use-package ace-window
  :bind (("M-o" . ace-window))
  :custom
  (aw-scope 'frame)
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  (aw-minibuffer-flag t)
  :config
  (ace-window-display-mode 1))

(use-package popper
  :bind (("C-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type)
         ("`" . popper-toggle-latest))
  :init
  (setq popper-group-function #'popper-group-by-perspective)
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          "^\\*Aweshell*" eshell-mode
          "^\\*vterm.*\\*$" vterm-mode
          "^\\*shell.*\\*$"  shell-mode 
          "^\\*term.*\\*$"   term-mode
          help-mode
          helpful-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

(use-package async
  :custom
  (dired-async-mode 1)
  (async-bytecomp-package-mode 1))

(use-package rainbow-mode
  :defer t
  :hook (org-mode
         emacs-lisp-mode
         web-mode
         typescript-mode
         js2-mode))

(setq-default tab-width 2)

(setq-default indent-tabs-mode nil)

(use-package guix)

(use-package lsp-mode
    :diminish "LSP"
    :ensure t
    :hook ((lsp-mode . lsp-diagnostics-mode)
           (lsp-mode . lsp-enable-which-key-integration)
           ((tsx-ts-mode
             typescript-ts-mode
             js-ts-mode) . lsp-deferred)
           (python-ts-mode . lsp-deferred)
           (go-ts-mode . lsp-deferred))
    :custom
    (lsp-keymap-prefix "C-c l")           ; Prefix for LSP actions
    (lsp-completion-provider :none)       ; Using Corfu as the provider
    (lsp-diagnostics-provider :flymake)
    (lsp-session-file (locate-user-emacs-file ".lsp-session"))
    (lsp-log-io nil)                      ; IMPORTANT! Use only for debugging! Drastically affects performance
    (lsp-keep-workspace-alive nil)        ; Close LSP server if all project buffers are closed
    (lsp-idle-delay 0.5)                  ; Debounce timer for `after-change-function'
    ;; core
    (lsp-enable-xref t)                   ; Use xref to find references
    (lsp-auto-configure t)                ; Used to decide between current active servers
    (lsp-eldoc-enable-hover t)            ; Display signature information in the echo area
    (lsp-enable-dap-auto-configure t)     ; Debug support
    (lsp-enable-file-watchers nil)
    (lsp-enable-folding nil)              ; I disable folding since I use origami
    (lsp-enable-imenu t)
    (lsp-enable-indentation nil)          ; I use prettier
    (lsp-enable-links nil)                ; No need since we have `browse-url'
    (lsp-enable-on-type-formatting nil)   ; Prettier handles this
    (lsp-enable-suggest-server-download t) ; Useful prompt to download LSP providers
    (lsp-enable-symbol-highlighting t)     ; Shows usages of symbol at point in the current buffer
    (lsp-enable-text-document-color nil)   ; This is Treesitter's job

    (lsp-ui-sideline-show-hover nil)      ; Sideline used only for diagnostics
    (lsp-ui-sideline-diagnostic-max-lines 20) ; 20 lines since typescript errors can be quite big
    ;; completion
    (lsp-completion-enable t)
    (lsp-completion-enable-additional-text-edit t) ; Ex: auto-insert an import for a completion candidate
    (lsp-enable-snippet t)                         ; Important to provide full JSX completion
    (lsp-completion-show-kind t)                   ; Optional
    ;; headerline
    (lsp-headerline-breadcrumb-enable t)  ; Optional, I like the breadcrumbs
    (lsp-headerline-breadcrumb-enable-diagnostics nil) ; Don't make them red, too noisy
    (lsp-headerline-breadcrumb-enable-symbol-numbers nil)
    (lsp-headerline-breadcrumb-icons-enable nil)
    ;; modeline
    (lsp-modeline-code-actions-enable nil) ; Modeline should be relatively clean
    (lsp-modeline-diagnostics-enable nil)  ; Already supported through `flycheck'
    (lsp-modeline-workspace-status-enable nil) ; Modeline displays "LSP" when lsp-mode is enabled
    (lsp-signature-doc-lines 1)                ; Don't raise the echo area. It's distracting
    (lsp-ui-doc-use-childframe t)              ; Show docs for symbol at point
    (lsp-eldoc-render-all nil)            ; This would be very useful if it would respect `lsp-signature-doc-lines', currently it's distracting
    ;; lens
    (lsp-lens-enable nil)                 ; Optional, I don't need it
    ;; semantic
    (lsp-semantic-tokens-enable nil)      ; Related to highlighting, and we defer to treesitter

    :preface
    (defun lsp-booster--advice-json-parse (old-fn &rest args)
      "Try to parse bytecode instead of json."
      (or
       (when (equal (following-char) ?#)
         
         (let ((bytecode (read (current-buffer))))
           (when (byte-code-function-p bytecode)
             (funcall bytecode))))
       (apply old-fn args)))
    (defun lsp-booster--advice-final-command (old-fn cmd &optional test?)
      "Prepend emacs-lsp-booster command to lsp CMD."
      (let ((orig-result (funcall old-fn cmd test?)))
        (if (and (not test?)                             ;; for check lsp-server-present?
                 (not (file-remote-p default-directory)) ;; see lsp-resolve-final-command, it would add extra shell wrapper
                 lsp-use-plists
                 (not (functionp 'json-rpc-connection))  ;; native json-rpc
                 (executable-find "emacs-lsp-booster"))
            (progn
              (message "Using emacs-lsp-booster for %s!" orig-result)
              (cons "emacs-lsp-booster" orig-result))
          orig-result)))
    :init
    (advice-add (if (progn (require 'json)
                           (fboundp 'json-parse-buffer))
                    'json-parse-buffer
                  'json-read)
                :around
                #'lsp-booster--advice-json-parse)
    (advice-add 'lsp-resolve-final-command :around #'lsp-booster--advice-final-command)
    )

  (use-package lsp-completion
    :no-require
    :hook ((lsp-mode . lsp-completion-mode)))

  (use-package lsp-ui
    :ensure t
    :commands
    (lsp-ui-doc-show
     lsp-ui-doc-glance)
    :bind (:map lsp-mode-map
                ("C-c C-d" . 'lsp-ui-doc-glance))
    :after (lsp-mode)
    :config (setq lsp-ui-doc-enable t
                  lsp-ui-doc-show-with-cursor nil      ; Don't show doc when cursor is over symbol - too distracting
                  lsp-ui-doc-include-signature t       ; Show signature
                  lsp-ui-doc-position 'at-point))

(use-package flymake
  :config
  (add-hook 'flymake-mode 'flymake-flycheck-auto))

(use-package flymake-collection
  :after flymake
  :hook (after-init . flymake-collection-hook-setup))

(use-package flymake-shellcheck
  :commands flymake-shellcheck-load
  :after flymake
  :hook
  (sh-mode . flymake-shellcheck-load))

(use-package lsp-eslint
    :demand t
    :after lsp-mode)

(use-package python-mode
    :flymake-hook
    (python-mode
     flymake-collection-mypy                      ; Always added to diagnostic functions.
     (flymake-collection-pycodestyle :disabled t) ; Never added.
     (flymake-collection-pylint)
     (flymake-collection-bandit)))

(use-package combobulate
  :preface
  (setq combobulate-key-prefix "C-c o")
  :hook
  ((python-ts-mode . combobulate-mode)
   (js-ts-mode . combobulate-mode)
   (go-mode . go-ts-mode)
   (html-ts-mode . combobulate-mode)
   (css-ts-mode . combobulate-mode)
   (yaml-ts-mode . combobulate-mode)
   (typescript-ts-mode . combobulate-mode)
   (json-ts-mode . combobulate-mode)
   (tsx-ts-mode . combobulate-mode)))

(use-package treesit-auto
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(with-eval-after-load 'treesit
  (setq treesit-extra-load-path '("/home/chris0ax/.guix-home/profile/lib/tree-sitter/")))
(use-package treesit-fold)

(use-package markdown-mode
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "multimarkdown")
  :bind (:map markdown-mode-map
         ("C-c C-e" . markdown-do)))

(use-package dape
  :preface
;; By default dape shares the same keybinding prefix as `gud'
;; If you do not want to use any prefix, set it to nil.
  (setq dape-key-prefix "\C-x\C-a")

  :hook
  ;; Save breakpoints on quit
  ((kill-emacs . dape-breakpoint-save)
   ;; Load breakpoints on startup
   (after-init . dape-breakpoint-load))
  
  :init
  ;; To use window configuration like gud (gdb-mi)
  (setq dape-buffer-window-arrangement 'gud)
  
  :config
  ;; Info buffers to the right
  (setq dape-buffer-window-arrangement 'right)

  ;; Global bindings for setting breakpoints with mouse
  (dape-breakpoint-global-mode)

  ;; Pulse source line (performance hit)
  (add-hook 'dape-display-source-hook 'pulse-momentary-highlight-one-line)

  ;; To not display info and/or buffers on startup
  (remove-hook 'dape-start-hook 'dape-info)
  (remove-hook 'dape-start-hook 'dape-repl)

  ;; To display info and/or repl buffers on stopped
  (add-hook 'dape-stopped-hook 'dape-info)
  (add-hook 'dape-stopped-hook 'dape-repl)

  ;; Kill compile buffer on build success
  (add-hook 'dape-compile-hook 'kill-buffer)

  ;; Save buffers on startup, useful for interpreted languages
  (add-hook 'dape-start-hook (lambda () (save-some-buffers t t)))
  
  ;; Projectile users
  (setq dape-cwd-fn 'projectile-project-root)
  )

(use-package apheleia
  :config
  (apheleia-global-mode +1))

(use-package nixpkgs-fmt
  :after (nix-mode))

(use-package nix-mode)

(use-package envrc
  :hook (after-init . envrc-global-mode))
(with-eval-after-load 'envrc
  (define-key envrc-mode-map (kbd "C-x e") 'envrc-command-map))

(use-package org-nix-shell
 :hook (org-mode . org-nix-shell-mode))

(defun my--init-nim-mode ()
  "Local init function for `nim-mode'."
  
  ;; Just an example, by default these functions are
  ;; already mapped to "C-c <" and "C-c >".
  (local-set-key (kbd "M->") 'nim-indent-shift-right)
  (local-set-key (kbd "M-<") 'nim-indent-shift-left)
  
  ;; Make files in the nimble folder read only by default.
  ;; This can prevent to edit them by accident.
  (when (string-match "/\.nimble/" (or (buffer-file-name) "")) (read-only-mode 1))

  ;; If you want to experiment, you can enable the following modes by
  ;; uncommenting their line.
  ;; (nimsuggest-mode 1)
  ;; Remember: Only enable either `flycheck-mode' or `flymake-mode' at the same time.
  ;; (flycheck-mode 1)
  (flymake-mode 1)

  ;; The following modes are disabled for Nim files just for the case
  ;; that they are enabled globally.
  ;; Anything that is based on smie can cause problems.
  (auto-fill-mode 0)
  (electric-indent-local-mode 0)
  )
(use-package nim-mode
  :hook
  (nim-mode . cax/init-nim-mode)
  :config
  ;(setq nimsuggest-path "")
  )

(use-package ob-nim)

(electric-pair-mode)
(use-package racket-mode)

(use-package flymake-racket
  :commands (flymake-racket-add-hook)
  :init
  (add-hook 'racket-mode-hook #'flymake-racket-add-hook))

(use-package paredit
  :hook
  (emacs-lisp-mode . enable-paredit-mode)
  (racket-mode . enable-paredit-mode)
  (eval-expression-minibuffer-setup-hook . enable-paredit-mode)
  (ielm-mode . enable-paredit-mode)
  (lisp-mode . enable-paredit-mode)
  (lisp-interaction-mode . enable-paredit-mode)
  (scheme-mode . enable-paredit-mode))

(use-package sly
  :hook
  (sly-mrepl-mode . enable-paredit-mode))

(use-package geiser)

(use-package geiser-guile)

(use-package plantuml-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
  :config
  (setq plantuml-executable-path "/run/current-system/sw/bin/plantuml")
  (setq plantuml-default-exec-mode 'executable))

(use-package go-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode)))

(use-package yaml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))

(use-package yaml-pro
 :config
 (add-hook 'yaml-mode-hook 'yaml-pro-ts-mode 100))

(use-package json-mode)

(use-package clojure-mode
    :hook
    (clojure-mode . subword-mode)
    (clojure-mode . paredit-mode)
    :config
    (add-to-list 'auto-mode-alist '("\\.boot$" . clojure-mode))
    (add-to-list 'auto-mode-alist '("\\.cljs.*$" . clojure-mode))
    (add-to-list 'auto-mode-alist '("lein-env" . enh-ruby-mode))
)

(defun cider-start-http-server ()
  (interactive)
  (cider-load-buffer)
  (let ((ns (cider-current-ns)))
    (cider-repl-set-ns ns)
    (cider-interactive-eval (format "(println '(def server (%s/start))) (println 'server)" ns))
    (cider-interactive-eval (format "(def server (%s/start)) (println server)" ns))))

(defun cider-refresh ()
  (interactive)
  (cider-interactive-eval (format "(user/reset)")))

(defun cider-user-ns ()
  (interactive)
  (cider-repl-set-ns "user"))

(use-package cider
             :bind
             ("C-c u" . cider-user-ns)
             ("C-M-r" . cider-refresh)
             :hook
             (cider-repl-mode . paredit-mode)
             :config
             (setq cider-auto-select-error-buffer t)
             (setq cider-show-error-buffer t)
             (setq cider-repl-history-file "~/.emacs.d/cider-history")
             (setq cider-repl-pop-to-buffer-on-connect t)
             (setq cider-repl-wrap-history t)
             )

(use-package clj-refactor
             :config
             (cljr-add-keybindings-with-prefix "C-c C-m")
             :hook
             (clojure-mode . clj-refactor-mode))

(use-package terraform-mode
  :custom (terraform-indent-level 4)
  :config
  (defun my-terraform-mode-init ()
    (outline-minor-mode 1)
    )
  (add-hook 'terraform-mode-hook 'my-terraform-mode-init))

(use-package ansible)

(defun dw/switch-project-action ()
  "Switch to a workspace with the project name."
  (persp-switch (projectile-project-name)))

  (use-package projectile
    :diminish projectile-mode
    :config (projectile-mode)
    :demand t
    :bind ("C-M-p" . projectile-find-file)
    :bind-keymap
    ("C-c p" . projectile-command-map)
    :init
    (when (file-directory-p "~/Projects/Code")
      (setq projectile-project-search-path '("~/Projects/Code"
                                             "~/Syncthing/Sync-Essentials/books"
                                             "~/Syncthing/Documents/Org-Notes/"
                                             "~/Syncthing/Documents/Org-Roam-Notes/"))
    (setq projectile-switch-project-action #'dw/switch-project-action)))

(use-package consult-projectile
  :after projectile)

(use-package magit
  :bind ("C-M-;" . magit-status-here)
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package forge
  :after magit)

(use-package tramp
  :config
  ;; Enable full-featured Dirvish over TRAMP on certain connections
  ;; https://www.gnu.org/software/tramp/#Improving-performance-of-asynchronous-remote-processes-1.
  ;; Tips to speed up connections
  (setq tramp-verbose 1)
  (setq tramp-chunksize 2000)
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
  (setq tramp-use-ssh-controlmaster-options nil))

(use-package ssh-deploy
  :after hydra
  :hook ((after-save . ssh-deploy-after-save)
             (find-file . ssh-deploy-find-file))
      :config
      (ssh-deploy-line-mode) ;; If you want mode-line feature
      (ssh-deploy-add-menu) ;; If you want menu-bar feature
      (ssh-deploy-hydra "C-c C-z"))

(use-package dired
  :config
  (setq dired-mouse-drag-files t))

(use-package dirvish
  :init
  (dirvish-override-dired-mode)
  :custom
  (dirvish-quick-access-entries ; It's a custom option, `setq' won't work
   '(("h" "~/"                          "Home")
     ("d" "~/Downloads/"                "Downloads")
     ("p" "~/Projects/Code"             "Projects")
     ("s" "~/Syncthing"                 "Syncthing")
     ("m" "/mnt/"                       "Drives")
     ("t" "~/.local/share/Trash/files/" "TrashCan")))
  :config
  (setq dirvish-hide-details nil)
  (setq dirvish-attributes
    '(all-the-icons file-size subtree-state vc-state git-msg))
  (setq dirvish-mode-line-format
      '(:left (sort symlink file-user) :right (omit yank index file-size)))
  (setq dired-listing-switches
        "-l --almost-all --human-readable --group-directories-first --no-group")
  :bind ; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
  (("C-c f" . dirvish-fd)
   :map dirvish-mode-map ; Dirvish inherits `dired-mode-map'
   ("a"   . dirvish-quick-access)
   ("f"   . dirvish-file-info-menu)
   ("y"   . dirvish-yank-menu)
   ("N"   . dirvish-narrow)
   ("^"   . dirvish-history-last)
   ("h"   . dirvish-history-jump) ; remapped `describe-mode'
   ("s"   . dirvish-quicksort)    ; remapped `dired-sort-toggle-or-edit'
   ("v"   . dirvish-vc-menu)      ; remapped `dired-view-file'
   ("TAB" . dirvish-subtree-toggle)
   ("M-f" . dirvish-history-go-forward)
   ("M-b" . dirvish-history-go-backward)
   ("M-l" . dirvish-ls-switches-menu)
   ("M-m" . dirvish-mark-menu)
   ("M-t" . dirvish-layout-toggle)
   ("M-s" . dirvish-setup-menu)
   ("M-e" . dirvish-emerge-menu)
   ("M-j" . dirvish-fd-jump)
   ("<mouse-1>" . dirvish-subtree-toggle-or-open)
   ("<mouse-2>" . dired-mouse-find-file-other-window)
   ("<mouse-3>" . dired-mouse-find-file)))

(use-package dired-rsync
  :bind
  (:map dired-mode-map
        ("C-c C-r" . dired-rsync)))

(use-package diredfl
  :hook
  ((dired-mode . diredfl-mode)
   ;; highlight parent and directory preview as well
   (dirvish-directory-view-mode . diredfl-mode))
  :config
  (set-face-attribute 'diredfl-dir-name nil :bold t))

(use-package openwith
  :config
  (setq openwith-associations
    (list
      (list (openwith-make-extension-regexp
              '("mpg" "mpeg" "mp3" "mp4"
                "avi" "wmv" "wav" "mov" "flv"
                "ogm" "ogg" "mkv"))
                "mpv"
                '(file))
      ))
  (openwith-mode 1))

(use-package eat
  :config
  (add-hook 'eshell-load-hook #'eat-eshell-mode)
  (add-hook 'eshell-load-hook #'eat-eshell-visual-command-mode)
  :bind
  ("C-c e e" . eat))

(use-package exec-path-from-shell)

(dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "GUIX_ENVIRONMENT" "GUIX_PYTHONPATH" "LSP_USE_PLISTS"))
    (add-to-list 'exec-path-from-shell-variables var))

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
(when (daemonp)
  (exec-path-from-shell-initialize))

(use-package term
  :commands term
  :config
  (setq explicit-shell-file-name "bash"))
(use-package eterm-256color
  :hook (term-mode . eterm-256color-mode))

(use-package vterm
  :commands vterm
  :config
  (setq vterm-shell "bash")
  (setq vterm-max-scrollback 10000))

(defun efs/configure-eshell ()
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
  (setq eshell-history-size 10000
    eshell-buffer-maximum-lines 10000
    eshell-hist-ignoredups t
    eshell-scroll-to-bottom-on-input t))

(defun corfu-send-shell (&rest _)
  "Send completion candidate when inside comint/eshell."
  (cond
  ((and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
    (eshell-send-input))
  ((and (derived-mode-p 'comint-mode)  (fboundp 'comint-send-input))
    (comint-send-input))))

(use-package eshell-prompt-extras
  :after eshell)

(with-eval-after-load 'eshell
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-highlight-prompt nil
        eshell-prompt-function 'epe-theme-lambda))

(use-package eshell-up
  :after eshell)


(use-package eshell
  :hook
  (eshell . efs/configure-eshell)
  :config
  (advice-add #'corfu-insert :after #'corfu-send-shell)
  (add-hook 'eshell-mode-hook
        (lambda ()
          (setq-local
           corfu-quit-at-boundary t
           corfu-quit-no-match t
           corfu-auto nil)
          (corfu-mode)) nil t)
  (setq eshell-aliases-file "~/.emacs.d/.eshell.aliases")
  (setq eshell-destroy-buffer-when-process-dies t)
  (setq eshell-visual-commands '("htop" "zsh" "vim" "gpg")))

(use-package bash-completion
  :config
  (autoload 'bash-completion-dynamic-complete
    "bash-completion"
    "BASH completion hook")
  (add-hook 'shell-dynamic-complete-functions
            'bash-completion-dynamic-complete))

(defun efs/org-font-setup ()
  (set-face-attribute 'org-document-title nil :font "FiraMono Nerd Font Propo" :weight 'bold :height 1.5)

  (dolist (face '((org-level-1 . 1.75)
                  (org-level-2 . 1.5)
                  (org-level-3 . 1.25)
                  (org-level-4 . 1.1)
                  (org-level-5 . 1.0)
                  (org-level-6 . 1.0)
                  (org-level-7 . 1.0)
                  (org-level-8 . 1.0)))
  (set-face-attribute (car face) nil :font "FiraMono Nerd Font Propo" :weight 'regular :height (cdr face)))
  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (custom-theme-set-faces
   'user
   '(org-block ((t (:inherit fixed-pitch :foreground unspecified))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-checkbox ((t (:inherit fixed-pitch))))
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table ((t (:inherit fixed-pitch))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))))

(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1)
  )

(use-package org
  :commands (org-capture org-agenda)
  :hook
  (org-mode . efs/org-mode-setup)
  :bind
  ("C-c a" . org-agenda)
  :config
  ; Hide tags of entries of org agenda
  (setq org-agenda-hide-tags-regexp ".*")
  (setq org-hide-emphasis-markers t)
  (setq org-ellipsis " ▾")
  (setq org-pretty-entities t)
  (setq org-use-sub-superscripts "{}")
  (setq org-startup-indented t)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-return-follows-link t)
  (setq org-agenda-files
        '("~/Syncthing/Documents/Org-Notes/Agenda"))
  (setq org-todo-keywords
    '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
      (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))

  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60)

  (setq org-tag-alist
    '((:startgroup)
       ; Put mutually exclusive tags here
       (:endgroup)
       ("@errand" . ?E)
       ("@home" . ?H)
       ("@work" . ?W)
       ("agenda" . ?a)
       ("planning" . ?p)
       ("publish" . ?P)
       ("batch" . ?b)
       ("note" . ?n)
       ("idea" . ?i)))

  (setq org-refile-targets
          '(("Archive.org" :maxlevel . 1)))

    ;; Save org buffers after refiling
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  (setq org-capture-templates
    `(("t" "Tasks / Projects")
      ("tt" "Task" entry (file+olp "~/Syncthing/Documents/Org-Notes/Agenda/Tasks.org" "Inbox")
           "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

      ("j" "Journal Entries")
      ("jj" "Journal" entry
           (file+olp+datetree "~/Syncthing/Documents/Org-Notes/Journal/Journal.org")
           "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
           ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
           :clock-in :clock-resume
           :empty-lines 1)
      ("jm" "Meeting" entry
           (file+olp+datetree "~/Syncthing/Documents/Org-Notes/Journal/Meetings.org")
           "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
           :clock-in :clock-resume
           :empty-lines 1)

      ("w" "Workflows")
      ("we" "Checking Email" entry (file+olp+datetree "~/Syncthing/Documents/Org-Notes/Journal/Workflows.org")
           "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

      ("m" "Metrics Capture")
      ("mw" "Weight" table-line (file+headline "~/Syncthing/Documents/Org-Notes/Journal/Metrics.org" "Weight")
       "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)))

  (define-key global-map (kbd "C-c j")
              (lambda () (interactive) (org-capture nil "jj")))
 (efs/org-font-setup))

(use-package org-super-agenda
  :after org
  :custom
  (org-super-agenda-mode +1)
  :config
  (setq org-super-agenda-groups
        '(
          (:name "Routine"
                 :habit t
                 :order 1)
          (:name "General Todo"
                 :tag "general"
                 :order 3)
          (:name "University Related Todo"
                 :tag "uni"
                 :order 4)
          (:name "Learning"
                 :tag "learning"
                 :order 4)
          (:name "Self Hosting"
                 :tag "self_hosting"
                 :order 5)
          (:name "System Crafting"
                 :tag "system_crafting"
                 :order 5)
          (:name "Events Today"
                 :tag "event"
                 :order 2)
          (:name "Reading"
                 :and (:todo "TO-READ" :tag ("book" "article" "manga")))
          (:discard (:anything t))
          (:name "Going to Watch"
                 :todo "TO-WATCH")
          (:name "Important"
                 :priority "A"
                 :order 1)
          ))
  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
   '(("d" "Dashboard"
     ((agenda "" ((org-agenda-span 'day)
                  (org-super-agenda-groups
                   '(
                     ; Habit only groups items with PROPERTY STYLE:habit
                     (:name "Habit"
                            :habit t
                            :scheduled today)
                     (:name "Today"
                            :todo "TODAY"
                            :date today
                            :scheduled today)
                     (:name "Due Today"
                            :time-grid t
                            :deadline today)
                     (:name "Events Today"
                            :time-grid t
                            :tag "event"
                            :scheduled today)
                     (:name "Overdue"
                            :deadline past)
                     (:name "Due Soon"
                            :deadline future)
                     (:name "Events Coming Soon"
                            :tag "event"
                            :scheduled future)
                     (:name "Done today"
                            :and (:regexp "State \"DONE\""
                                          :log t))
                     (:name "Clocked today"
                            :log t)
                     ))
                  ))
      (alltodo "" ((org-agenda-overriding-header "")))
     (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
      (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))

    ("W" "Work Tasks" tags-todo "+work-email")
    
    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))
    ("w" "Workflow Status"
      ((todo "WAIT"
            ((org-agenda-overriding-header "Waiting on External")
             (org-agenda-files org-agenda-files)))
      (todo "REVIEW"
            ((org-agenda-overriding-header "In Review")
             (org-agenda-files org-agenda-files)))
      (todo "PLAN"
            ((org-agenda-overriding-header "In Planning")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "BACKLOG"
            ((org-agenda-overriding-header "Project Backlog")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "READY"
            ((org-agenda-overriding-header "Ready for Work")
             (org-agenda-files org-agenda-files)))
      (todo "ACTIVE"
            ((org-agenda-overriding-header "Active Projects")
             (org-agenda-files org-agenda-files)))
      (todo "COMPLETED"
            ((org-agenda-overriding-header "Completed Projects")
             (org-agenda-files org-agenda-files)))
      (todo "CANC"
            ((org-agenda-overriding-header "Cancelled Projects")
             (org-agenda-files org-agenda-files)))))))
                   )))

;(use-package org-appear
 ; :after org
 ; :hook
 ; (org-mode . org-appear-mode))

; org-bullets is no longer maintained, using its successor
(use-package org-superstar
  :after org
  :hook (org-mode . org-superstar-mode)
  :config
  ;(org-superstar-configure-like-org-bullets)
  (setq org-superstar-remove-leading-stars t)
  (setq org-superstar-headline-bullets-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org-fragtog
    :after org
    :custom
    (org-startup-with-latex-preview t)
    :hook
    (org-mode . org-fragtog-mode)
    :custom
    (org-format-latex-options
     (plist-put org-format-latex-options :scale 2)
     (plist-put org-format-latex-options :foreground 'auto)
     (plist-put org-format-latex-options :background 'auto)))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :defer t
  :hook (org-mode . efs/org-mode-visual-fill)
  :config
  (add-hook 'org-agenda-finalize-hook #'visual-fill-column-mode))

(use-package org-modern
  :custom
  (org-modern-list 
   '((?- . "-")
     (?* . "•")
     (?+ . "‣")))
  (org-modern-block-name '("" . ""))
  :custom
  (org-modern-table nil)
  :config
  (add-hook 'org-agenda-finalize-hook #'org-modern-agenda)
  :hook
  (org-mode . org-modern-mode))
  

(use-package org-modern-indent
  :demand t
  :config
  (add-hook 'org-mode-hook #'org-modern-indent-mode 90))

(use-package ob-mermaid
  :after org
  :config
  (setq ob-mermaid-cli-path "/run/current-system/sw/bin/mmdc"))

(setq org-babel-lisp-eval-fn 'sly-eval)
(push '("conf-unix" . conf-unix) org-src-lang-modes)
(push '("plantuml" . plantuml) org-src-lang-modes)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (java . t)
   (mermaid . t)
   (plantuml . t)
   (nim . t)
   (lisp . t)))

(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

(with-eval-after-load 'org
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python")))
(add-to-list 'org-structure-template-alist '("li" . "src lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))

(use-package org-side-tree
  :after org)

(use-package org-roam
  :after org
  :custom
  (org-roam-directory (file-truename "~/Syncthing/Documents/Org-Roam-Notes/"))
  (org-roam-dailies-directory "Journal/")
  (setq org-roam-complete-everywhere t)
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  (org-roam-capture-templates
    '(("d" "default" plain
       "%?"
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
       :unnarrowed t)
      ("b" "book notes" plain
       (file "~/Syncthing/Documents/Org-Roam-Notes/Templates/BookNote.org")
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
      ("p" "project file" plain
       "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add Initial tasks \n\n * Dates \n\n"
       ; Add a tag of project to make it easy to query and filter org-roam notes
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;;Dailies
         ("C-c n j" . org-roam-dailies-capture-today)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  (require 'org-roam-dailies)
  (require 'org-roam-protocol))

(use-package org-roam-ui
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(use-package org-pomodoro
  :after org
  :config
  (setq org-pomodoro-start-sound "~/Projects/Code/system-conf/sounds/focus_bell.wav")
  (setq org-pomodoro-short-break-sound "~/Project/Code/system-conf/sounds/three_beeps.wav")
  (setq org-pomodoro-long-break-sound "~/Projects/Code/system-conf/sounds/three_beeps.wav")
  (setq org-pomodoro-finished-sound "~/Projects/Code/system-conf/sounds/meditation_bell.wav"))

(use-package org-download
  :config
  (add-hook 'dired-mode-hook 'org-download-enable))

(use-package org-cliplink
  :after org
  :bind
  ("C-x p i" . org-cliplink))

(defun org-image-link (protocol link _description)
  "Interpret LINK as base64-encoded image data."
  (cl-assert (string-match "\\`img" protocol) nil
             "Expected protocol type starting with img")
  (let ((buf (url-retrieve-synchronously (concat (substring protocol 3) ":" link))))
    (cl-assert buf nil
               "Download of image \"%s\" failed." link)
    (with-current-buffer buf
      (goto-char (point-min))
      (re-search-forward "\r?\n\r?\n")
      (buffer-substring-no-properties (point) (point-max)))))

(use-package org-transclusion
  :after org
  :config
  (define-key global-map (kbd "<f12>") #'org-transclusion-add))

(use-package toc-org
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

(with-eval-after-load 'markdown-mode
  (define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point))

;(use-package jinx
 ;  :hook (emacs-startup . global-jinx-mode)
  ; :bind (("M-$" . jinx-correct)
   ;       ("C-M-$" . jinx-languages)))

; (use-package org-media-note
 ;  :after org
  ; :hook (org-mode .  org-media-note-mode)
;   :bind (
;          ("C-c o" . org-media-note-hydra/body))  ;; Main entrance
;   :config
;   (setq org-media-note-screenshot-image-dir "~/Syncthing/Documents/Org-Notes/imgs")  ;; Folder to save screenshot
; )

(use-package hledger-mode
    :after htmlize
    :load-path "packages/rest/hledger-mode/"
    :mode ("\\.journal\\'" "\\.hledger\\'")
    :commands hledger-enable-reporting
    :preface
    (defun hledger/next-entry ()
      "Move to next entry and pulse."
      (interactive)
      (hledger-next-or-new-entry)
      (hledger-pulse-momentary-current-entry))

    (defface hledger-warning-face
      '((((background dark))
         :background "Red" :foreground "White")
        (((background light))
         :background "Red" :foreground "White")
        (t :inverse-video t))
      "Face for warning"
      :group 'hledger)

    (defun hledger/prev-entry ()
      "Move to last entry and pulse."
      (interactive)
      (hledger-backward-entry)
      (hledger-pulse-momentary-current-entry))
    
    :bind (("C-c j" . hledger-run-command)
           :map hledger-mode-map
           ("C-c e" . hledger-jentry)
           ("M-p" . hledger/prev-entry)
           ("M-n" . hledger/next-entry))
    :init
    (setq hledger-jfile
          (expand-file-name "~/Syncthing/Sync-Essentials/finance/accounting.journal")
          hledger-email-secrets-file (expand-file-name "secrets.el"
                                                       emacs-assets-directory))
    ;; Expanded account balances in the overall monthly report are
    ;; mostly noise for me and do not convey any meaningful information.
    (setq hledger-show-expanded-report nil)

    (when (boundp 'my-hledger-service-fetch-url)
      (setq hledger-service-fetch-url
            my-hledger-service-fetch-url))

    :config
    (add-hook 'hledger-view-mode-hook #'hl-line-mode)
    (add-hook 'hledger-view-mode-hook #'center-text-for-reading)
    
    (add-hook 'hledger-view-mode-hook
              (lambda ()
                (run-with-timer 1
                                nil
                                (lambda ()
                                  (when (equal hledger-last-run-command
                                               "balancesheet")
                                    ;; highlight frequently changing accounts
                                    (highlight-regexp "^.*\\(savings\\|cash\\).*$")
                                    (highlight-regexp "^.*credit-card.*$"
                                                      'hledger-warning-face))))))
    
    (add-hook 'hledger-mode-hook
              (lambda ()
                (make-local-variable 'company-backends)
                (add-to-list 'company-backends 'hledger-company))))

(use-package hledger-input
  :load-path "packages/rest/hledger-mode/"
  :bind (("C-c e" . hledger-capture)
         :map hledger-input-mode-map
         ("C-c C-b" . popup-balance-at-point))
  :preface
  (defun popup-balance-at-point ()
    "Show balance for account at point in a popup."
    (interactive)
    (if-let ((account (thing-at-point 'hledger-account)))
        (message (hledger-shell-command-to-string (format " balance -N %s "
                                                          account)))
      (message "No account at point")))
  
  :config
  (setq hledger-input-buffer-height 20)
  (add-hook 'hledger-input-post-commit-hook #'hledger-show-new-balances)
  (add-hook 'hledger-input-mode-hook #'auto-fill-mode)
  (add-hook 'hledger-input-mode-hook
            (lambda ()
              (make-local-variable 'company-idle-delay)
              (setq-local company-idle-delay 0.1))))

(use-package flymake-hledger
  :after flymake)

(use-package shrface)
(with-eval-after-load 'nov
  (define-key nov-mode-map (kbd "<tab>") 'shrface-outline-cycle)
  (define-key nov-mode-map (kbd "S-<tab>") 'shrface-outline-cycle-buffer)
  (define-key nov-mode-map (kbd "C-t") 'shrface-toggle-bullets)
  (define-key nov-mode-map (kbd "C-j") 'shrface-next-headline)
  (define-key nov-mode-map (kbd "C-k") 'shrface-previous-headline)
  (define-key nov-mode-map (kbd "M-l") 'shrface-links-consult)
  (define-key nov-mode-map (kbd "M-h") 'shrface-headline-consult))

(setq browse-url-generic-program "nyxt")
(setq browse-url-firefox-program "librewolf")
(setq browse-url-browser-function 'browse-url-firefox)

(use-package elfeed
  :custom
  (elfeed-use-curl t)
  (elfeed-curl-extra-arguments '("--insecure"))
  (elfeed-db-directory
  (expand-file-name "elfeed" user-emacs-directory))
  (elfeed-show-entry-switch 'display-buffer)
  :config
  (setq elfeed-feeds '(""))
  :bind
  ("C-c w e" . elfeed))

(use-package elfeed-org
  :config
  (setq rmh-elfeed-org-files (list "~/Syncthing/Sync-Essentials/feeds/elfeed.org" "~/Syncthing/Sync-Essentials/feeds/elfeed-youtube.org" ;"~/Syncthing/Sync-Essentials/feeds/elfeed-podcasts.org"
                              ))
  (elfeed-org)
  )

(use-package elfeed-goodies
  :after elfeed
  :config
  (elfeed-goodies/setup))

(defun elfeed-protocol-advice-rmh-elfeed-org-export-feed (headline)
 "Advice for `rmh-elfeed-org-export-feed', add elfeed-protocol ID as suffix for each feed."
  (let* ((url (car headline))
         (proto-id (car (elfeed-protocol-feed-list))))
    (when proto-id
      (setcar headline (elfeed-protocol-format-subfeed-id proto-id url)))))
(use-package elfeed-protocol
  :commands (elfeed)
  :custom
  (elfeed-protocol-fever-fetch-category-as-tag t)
  (elfeed-protocol-fever-update-unread-only t)
  (elfeed-protocol-enabled-protocols '(fever))
  :config
  (setq elfeed-protocol-feeds '(("fever+https://admin@rss.rathole.dedyn.io"
                                 :api-url "https://rss.rathole.dedyn.io/api/fever.php"
                                 :password-file "~/.freshrss.gpg"
                                 )))
  (setq elfeed-protocol-feeds (append elfeed-protocol-feeds elfeed-feeds))
  (advice-add 'rmh-elfeed-org-export-feed :before #'elfeed-protocol-advice-rmh-elfeed-org-export-feed)
  (elfeed-protocol-enable))

(use-package elfeed-autotag
  :after elfeed
  :config
  (setq elfeed-autotag-files '("~/Syncthing/Sync-Essentials/feeds/elfeed.org"
                               "~/Syncthing/Sync-Essentials/feeds/elfeed-youtube.org"
                               ;"~/Syncthing/Sync-Essentials/feeds/elfeed-podcasts.org"
                               ))
  (elfeed-autotag))

(use-package elfeed-tube
  :after elfeed
  :demand t
  :config
  ;; (setq elfeed-tube-auto-save-p nil) ; default value
  ;; (setq elfeed-tube-auto-fetch-p t)  ; default value
  (elfeed-tube-setup)

  :bind (:map elfeed-show-mode-map
         ("F" . elfeed-tube-fetch)
         ("C-c C-f" . elfeed-tube-mpv-follow-mode)
         ("C-c C-w" . elfeed-tube-mpv-where)
         ([remap save-buffer] . elfeed-tube-save)
         :map elfeed-search-mode-map
         ("F" . elfeed-tube-fetch)
         ([remap save-buffer] . elfeed-tube-save)))

(use-package elfeed-tube-mpv
  :after elfeed
  :bind (:map elfeed-show-mode-map
              ("C-c C-f" . elfeed-tube-mpv-follow-mode)
              ("C-c C-w" . elfeed-tube-mpv-where)))

;  (use-package elfeed-summary
;    :config
;    (define-key elfeed-summary-mode-map (kbd "R") #'elfeed-update)
;    (setq elfeed-summary-settings
;          '(
;            (group
;             (:title . "Searches")
;             (:elements
;              (search
;               (:filter . "@7-days-ago +unread")
;               (:title . "Unread entries this week"))
;              (search
;               (:filter . "@7-days-ago +unread +podcasts")
;               (:title . "Podcasts Episodes Released this Week"))
;              (search
 ;              (:filter . "@7-days-ago +unread +news +youtube")
 ;              (:title . "General News that happened past week"))
  ;            (search
  ;             (:filter . "@7-days-ago +unread +news +infosec")
   ;            (:title . "News related to Infosec past week"))
   ;           (search
   ;;            (:filter . "@7-days-ago +unread +tech +forums")
    ;           (:title . "Whats happening on Hacker News and Lobster"))
    ;          (search
    ;           (:filter . "@2-weeks-ago +unread +privacy")
    ;           (:title . "Recent in Privacy"))
    ;          (search
     ;          (:filter . "@7-days-ago +unread +youtube +tech")
     ;          (:title . "Latest Technology in Youtube"))
     ;         (search
      ;         (:filter . "@2-weeks-ago +unread +youtube +linux")
     ;          (:title . "Latest Linux Videos"))
     ;         (search
     ;          (:filter . "@2-weeks-ago +unread +youtube +animation")
    ;           (:title . "Recent Stuff from Animation"))
   ;           (search
   ;;            (:filter . "@2-weeks-ago +unread +youtube +gaming")
   ;            (:title . "Recent Videos from Gaming"))
  ;            (search
  ;             (:filter . "@2-weeks-ago +unread +youtube +music")
   ;            (:title . "Recent from Musictube")
   ;            )
   ;           (search
   ;            (:filter  . "2-weeks-ago +unread +climate")
   ;            (:title . "Recent Climate News"))))
    ;        (group
    ;         (:title . "History, Politics")
    ;         (:elements
    ;          (group
     ;          (:title . "Bread")
     ;          (:elements
     ;           (query . (and youtube breadtube))))
     ;         (group
     ;          (:title . "General")
    ;           (:elements
     ;           (query . (and youtube (and history politics)))))))
     ;       (group
     ;        (:title . "Philosophy")
     ;        (:elements
     ;         (query . (and youtube philosophy))))
    ;        (group
    ;         (:title . "Religion and Mythology")
    ;         (:elements
    ;          (query . (and youtube (and religion mythology)))))
    ;        (group
    ;         (:title . "Podcasts")
    ;         (:elements
    ;          (auto-tags
    ;           (:source . (query . podcasts)))))
    ;        (group
    ;         (:title . "Entertainment")
    ;         (:elements
    ;          (group
    ;           (:title . "Music")
    ; ;          (:elements
    ;            (query . music)))
    ;          (group
    ;           (:title . "Animation")
    ;           (:elements
    ;            (query . animation)))
    ;          (group
    ;           (:title . "Gaming")
    ;           (:elements
    ;            (query . gaming)))))
    ;        (group
    ;         (:title . "Environment Related")
    ;         (:elements
    ;          (group
    ;           (:title . "Nature")
    ;           (:elements
    ;            (query . (and youtube nature))))
   ;           (group
  ;             (:title . "Food")
  ;             (:elements
  ;              (query . (and youtube food))))
 ;             (group
 ;              (:title . "Climate")
 ;              (:elements
 ;               (query . climate)))
 ;             (group
;               (:title . "Urban Planning")
;               (:elements
;                (query . (and youtube urban_planning))))))
;            (group
;             (:title . "All feeds")
;             (:elements
;              (query . :all)))
;            )
;          ))

(use-package empv
    :config
    (bind-key "C-c m" empv-map)
    (add-to-list 'empv-mpv-args "--ytdl-format=best")
    (setq empv-invidious-instance "https://iv.ggtyler.dev/api/v1"))

(with-eval-after-load 'embark (empv-embark-initialize-extra-actions))

(use-package org-web-tools
  :bind
  ("C-c w w" . org-web-tools-insert-link-for-url))

(use-package ement
  :hook
  (ement-connect . ement-notifications-mode)
  :custom
  (ement-save-sessions t)
  (ement-sessions-file "~/.cache/emacs/var/ement-sessions.el")
  )

(use-package mastodon-alt)

(use-package mastodon
  :config
  (require 'mastodon-alt)
  (mastodon-alt-tl-activate)
  (setq mastodon-instance-url "https://infosec.exchange"
        mastodon-active-user "schrizoidian"))

(with-eval-after-load 'erc-tls
  (setq erc-server "irc.ea.libera.chat"
        erc-nick "schrizoidian"
        erc-autojoin-channels-alist '(("irc.ea.libera.chat" "#systemcrafters" "#emacs"))
        erc-track-shorten-start 8
        erc-kill-buffer-on-part t
        erc-auto-query 'bury)
  (setopt erc-sasl-mechanism 'plain)
  (dolist (modules '(netsplit fill button match track completion readonly networks ring autojoin noncommands irccontrols move-to-prompt stamp menu list sasl))
    (add-to-list 'erc-modules modules)))

; (use-package telega
 ;  :hook
 ;  (telega . telega-notifications-mode)
 ;  :custom
  ; (telega-use-docker t))

;(use-package mu4e)

(use-package simple-httpd)

(use-package cdlatex)

(use-package org-contrib)
(use-package auctex
  :config
  (require 'ox-extra)
  (ox-extras-activate '(latex-header-blocks ignore-headlines))
  (setq org-latex-with-hyperref nil)
  (setq org-latex-logfiles-extensions
      (quote ("lof" "lot" "tex~" "aux" "idx" "log" "out" "toc" "nav" "snm" "vrb" "dvi" "fdb_latexmk" "blg" "brf" "fls" "entoc" "ps" "spl" "bbl" "xmpi" "run.xml" "bcf" "acn" "acr" "alg" "glg" "gls" "ist")))
)

(require 'ox-latex)

    (setq org-latex-packages-alist '(("" "listings")
                                     ("" "booktabs")
                                     ("AUTO" "polyglossia" t ("xelatex" "lualatex"))
                                     ("" "grffile")
                                     ("" "unicode-math")
                                     ("" "xcolor")))
    (setq org-latex-compiler "xelatex")
    (setq org-latex-pdf-process '("latexmk -xelatex -shell-escape -f %f"))
    (setq org-latex-listings t)
    (setq org-latex-tables-booktabs t)
    (setq org-latex-images-centered t)
    (setq org-latex-listings-options
          '(("basicstyle" "\\ttfamily")
            ("showstringspaces" "false")
            ("keywordstyle" "\\color{blue}\\textbf")
            ("commentstyle" "\\color{gray}")
            ("stringstyle" "\\color{green!70!black}")
            ("stringstyle" "\\color{red}")
            ("frame" "single")
            ("numbers" "left")
            ("numberstyle" "\\ttfamily")
            ("columns" "fullflexible")))
(setq org-latex-inputenc-alist '((\"utf8\" . \"utf8x\")))
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
                 '("koma-general"
                   "\\documentclass[11pt,captions=tableheading]{scrartcl}
    \\linespread{1.25}
    \\usepackage{fontspec}
    \\defaultfontfeatures{Mapping=tex-text, RawFeature={+zero}}
    \\setmainfont{Noto Sans}[BoldFont=*-Medium,ItalicFont=*-Italic]
    \\setsansfont{Noto Sans}[BoldFont=*-Medium,ItalicFont=*-Italic]
    \\setmonofont{Noto Sans Mono}[BoldFont=*-Medium,Scale=0.8]
    \\usepackage{fancyhdr}
    \\pagestyle{fancy}
    \\fancyhf{}
    \\fancyhead[L]{\\leftmark}  % Left header
    \\fancyhead[R]{\\thepage}  % Right header"
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(setq org-latex-default-class "koma-general")

  

(eval-after-load 'ox '(require 'ox-koma-letter))

(use-package bibtex
  :custom
  (bibtex-dialect 'BibTeX)
  (bibtex-user-optional-fields
   '(("keywords" "Keywords to describe the entry" "")
     ("file" "Link to a document file." "" )))
  (bibtex-align-at-equal-sign t))

(use-package gscholar-bibtex)

(use-package citar
  :custom
  (setq citar-bibliography '(
                             ("~/Syncthing/Documents/Org-Notes/Journal/Food-Waste-App/references.bib")
                             ))
  (citar-bibliography org-cite-global-bibliography)
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup)
  :bind
  (:map org-mode-map :package org ("C-c b" . #'org-cite-insert)))

(use-package citar-embark
  :after citar embark
  :no-require
  :config (citar-embark-mode))

(use-package biblio)

(use-package nov
  :init
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))

(use-package calibredb
  :defer t
  :config
  (setq calibredb-size-show t)
  (setq calibredb-format-character-icons t)
  (setq calibredb-root-dir "~/Syncthing/Sync-Essentials/Calibre")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (setq calibredb-library-alist '(("~/Syncthing/Sync-Essentials/books"
                                   )
                                  ("~/Syncthing/Sync-Essentials/Calibre"))))

; (use-package ox-odt
;   :custom
;   (org-odt-create-custom-desktop-file-p t))

(use-package pdf-tools
        :config
        (pdf-tools-install)
        (setq-default pdf-view-display-size 'fit-width)
        (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
        ;; Make PDF tools available to Emacs and used to view exported tex files
        (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
              TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
              TeX-source-correlate-start-server t)

        (add-hook 'TeX-after-compilation-finished-functions
                  #'TeX-revert-document-buffer)
        :custom
        (pdf-annot-activate-created-annotations t "automatically annotate highlights"))

(use-package pdf-view-restore
:after pdf-tools
:config
;; By default history file is kept in the same directory as document
;;(setq pdf-view-restore-filename "~/.emacs.d/.pdf-view-restore")
(add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode))

(use-package djvu)
(use-package org-noter)

;    (use-package org-pdftools
;      :hook (org-mode . org-pdftools-setup-link))
;    (use-package org-noter-pdftools
;    :after org-noter
 ;   :config
;    ;; Add a function to ensure precise note is inserted
;    (defun org-noter-pdftools-insert-precise-note (&optional toggle-no-questions)
;      (interactive "P")
;      (org-noter--with-valid-session
;       (let ((org-noter-insert-note-no-questions (if toggle-no-questions
;                                                     (not org-noter-insert-note-no-questions)
;                                                   org-noter-insert-note-no-questions))
;             (org-pdftools-use-isearch-link t)
;             (org-pdftools-use-freepointer-annot t))
;         (org-noter-insert-note (org-noter--get-precise-info)))))

    ;; fix https://github.com/weirdNox/org-noter/pull/93/commits/f8349ae7575e599f375de1be6be2d0d5de4e6cbf
;    (defun org-noter-set-start-location (&optional arg)
;      "When opening a session with this document, go to the current location.
;  With a prefix ARG, remove start location."
;      (interactive "P")
;      (org-noter--with-valid-session
;       (let ((inhibit-read-only t)
;             (ast (org-noter--parse-root))
;             (location (org-noter--doc-approx-location (when (called-interactively-p 'any) 'interactive))))
;         (with-current-buffer (org-noter--session-notes-buffer session)
;           (org-with-wide-buffer
;            (goto-char (org-element-property :begin ast))
;            (if arg
;                (org-entry-delete nil org-noter-property-note-location)
;              (org-entry-put nil org-noter-property-note-location
;                             (org-noter--pretty-print-location location))))))))
;    (with-eval-after-load 'pdf-annot
 ;     (add-hook 'pdf-annot-activate-handler-functions #'org-noter-pdftools-jump-to-note)))

(setq gc-cons-threshold (* 200000000))

(with-eval-after-load 'server
  (unless (server-running-p)
    (server-start)))
  (load-theme 'doom-ayu-dark t)
