{
  description = "A very basic flake";

  inputs = {
    # nixpkgs = {
    #   url = "github:NixOS/nixpkgs/nixos-23.11";
    # };
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
  };

  outputs = { self, nixpkgs, ... }: 
    let
      lib = nixpkgs.lib;
      system = "x86_64-linux";
    in {
     nixosConfigurations = {
       CoralPacific = lib.nixosSystem {
         inherit system;
         modules = [
           ./configuration.nix
           ./hardware-configuration.nix
         ];
       };
     };
  };
}
