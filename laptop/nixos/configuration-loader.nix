{ config, pkgs, ... }:

let
  # One method was to fetch Tarball of channel from nixpkgs-channels, this is deprecated and its recommended
  # to just use nixpkgs repo branches which has channels 
  # similar to adding a channel by nix-channel
  #unstable = import 
   # (builtins.fetchTarball https://github.com/nixos/nixpkgs-channels/archive/nixos-unstable.tar.gz)
    #{ config = config.nixpkgs.config; }; 
  unstable = import <nix-unstable> { config = { allowUnfree = true;  }; };
in
{
  imports =
    [ # Include the results of the hardware scan.
      /home/chris0ax/Projects/Code/system-conf/laptop/nixos/hardware-configuration.nix
      /home/chris0ax/Projects/Code/system-conf/laptop/nixos/configuration.nix
    ];
}
