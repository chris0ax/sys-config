#Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

# Trying to configure so that select software packages can be taken from unstable channel
# Answer was found in https://discourse.nixos.org/t/installing-only-a-single-package-from-unstable/5598/4 
# The above answer is more short but not as explicit
# https://www.reddit.com/r/NixOS/comments/a3w67x/comment/eb9vzbj/ : Pinning a specific revision of NixPkgs with hash
# I like the explicit way so I have referenced:
# https://www.joseferben.com/posts/installing_only_certain_packages_from_an_unstable_nixos_channel 
# https://stackoverflow.com/questions/48831392/how-to-add-nixos-unstable-channel-declaratively-in-configuration-nix 
# let
#   # One method was to fetch Tarball of channel from nixpkgs-channels, this is deprecated and its recommended
#   # to just use nixpkgs repo branches which has channels 
#   # similar to adding a channel by nix-channel
#   #unstable = import 
#    # (builtins.fetchTarball https://github.com/nixos/nixpkgs-channels/archive/nixos-unstable.tar.gz)
#     #{ config = config.nixpkgs.config; }; 
#   unstable = import <nix-unstable> { config = { allowUnfree = true;  }; };
# in
{

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking = {
    nameservers = [ "127.0.0.1" "::1" ];
    hostName = "CoralPacific";
    dhcpcd.extraConfig = "nohook resolv.conf";
    networkmanager = {
      enable = true;
      dns = "none";
    };
  };
  #networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Set your time zone.
  time.timeZone = "Asia/Dubai";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the XFCE Desktop Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.xfce.enable = true;

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "";
  };
  xdg.portal.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-xapp ];
  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };
  # Enable Bluetooth GUI management
  services.blueman.enable = true;
  # Configure pipewire wireplumber For bluetooth
  services.pipewire.wireplumber.configPackages = [
    (pkgs.writeTextDir
      "wireplumber/bluetooth.lua.d/51-bluez-config.lua" ''
      bluez_monitor.properties = {
        ["bluez5.enable-sbc-xq"] = true,
        ["bluez5.enable-msbc"] = true,
        ["bluez5.enable-hw-volume"] = true,
        ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
      }
    '')
  ];

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Iosevka" "IosevkaTerm" "FiraMono" ]; })
  ];
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.chris0ax = {
    isNormalUser = true;
    description = "chris0ax";
    extraGroups = [ "networkmanager" "wheel" "docker" "libvirtd" "adbusers" ];
    packages = with pkgs; [
      betterbird
      lutris
      zotero
      calibre
      qbittorrent
      lagrange
      nicotine-plus
      libreoffice-fresh
      zoom-us
      signal-desktop
      element-desktop
      anki
    ];
  };
  # Android shell
  programs.adb.enable = true;
  # Set up MTP so that File transfer is possible
  services.gvfs.enable = true;
  # Power Management
  powerManagement.enable = true;
  services.auto-cpufreq = {
    enable = true;
    settings = {
      battery = {
        governor = "powersave";
        turbo = "never";
      };
      charger = {
        governor = "performance";
        turbo = "auto";
      };
    };
  };
  # Setting up services on boot for eye health
  services.redshift = {
    enable = true;
    latitude = "25.076517999025697";
    longitude = "55.22822246772954";
  };
  services.safeeyes = {
    enable = true;
  };
  programs.steam.enable = true;

  programs.direnv = {
    package = pkgs.direnv;
    silent = false;
    loadInNixShell = true;
    direnvrcExtra = "";
    enable = true;
    nix-direnv = {
      enable = true;
      package = pkgs.nix-direnv;
    };
  };
  # Allow unfree packages
  nixpkgs.config = {
    allowUnfree = true;
  };

  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/emacs-overlay/archive/master.tar.gz;
      sha256 = "1snk747m7gsbf8krby49cncsdwm08za3n64mv87wlg41c1z5n8gq";
    }))
    (self: super: {
      mpv = super.mpv.override {
        scripts = [ self.mpvScripts.webtorrent-mpv-hook ];
      };
    })
  ];
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim-full # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    curl
    alacritty
    librewolf
    ungoogled-chromium
    git
    tmux
    keepassxc
    mpv
    ffmpeg_5-full
    winetricks
    wineWowPackages.stable
    evince
    feh
    i2p
    tor
    htop
    flameshot
    xdg-desktop-portal-xapp
    xfce.xfce4-whiskermenu-plugin
    xfce.xfce4-clipman-plugin
    xclip
    (jdk21.override { enableJavaFX = true; })
    obs-studio
    pavucontrol
    tor-browser
    fsearch
    # For Emacs empv.el
    fd
    veracrypt
    qalculate-gtk
    yt-dlp
    pandoc
    ripgrep
    localsend
    (python312Full.withPackages (ps: with ps; [ ds4drv ] ))
    jdt-language-server
    vscodium-fhs
    # Latex dependencies
    texliveFull
    texlab
    djvulibre
    poppler_utils
    zip
    ghostscript_headless
    mupdf
    imagemagick
    bleachbit
    wireshark
    mermaid-cli
    plantuml
    pdf4qt
    unzip
    pidgin
    syncthingtray
    #Kotlin
    kotlin
    kotlin-language-server
    android-studio
    # Offline tools
    goldendict-ng
    kiwix
    dialect
    rclone
    #mpv
    ff2mpv
    # Emacs email
    isync
    nuspell
    xorg.xmodmap
    hunspellDicts.en_US-large
    emacs-unstable
    pinentry-all
    stow
    gnupg
    OVMFFull
    micro
    guile
  ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nix.settings.keep-outputs = true;
  nix.settings.keep-derivations = true;
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:
  services.flatpak.enable = true;

  services.syncthing = {
    enable = true;
    user = "chris0ax";
    dataDir = "/home/chris0ax/Syncthing";
    configDir = "/home/chris0ax/.config/syncthing";
    overrideDevices = true;
    overrideFolders = true;
    settings = {
      devices = {
        "2201116PG" = { id = "SRCVRK3-65FOTZM-HYAK3MA-3GFWMEI-3ZMIYTM-LJB6J37-WQ42VTX-2PGHUAF"; };
        "server0ax" = { id = "R6K43ZI-FVCA4UJ-REI7KJT-UAZI5ZZ-6QG3USI-ETBKYDI-XEV7UPH-WUK6EQJ"; };
      };
      folders = {
        "Sync-Essentials" = {
          path = "/home/chris0ax/Syncthing/Sync-Essentials";
          devices = [ "2201116PG" "server0ax" ];
        };
        "Backup-Android" = {
          path = "/home/chris0ax/Syncthing/Backup-Android";
          devices = [ "2201116PG" "server0ax" ];
        };
        "Documents" = {
          path = "/home/chris0ax/Syncthing/Documents";
          devices = [ "2201116PG" "server0ax" ];
        };
        "Research" = {
          path = "/home/chris0ax/Syncthing/Research";
          devices = [ "2201116PG" "server0ax" ];
        };
      };
    };
  };
  ## Setting up Containers
  virtualisation.docker = {
    enable = true;
    storageDriver = "btrfs";
  };

  virtualisation.libvirtd = {
    enable = true;
  };
  programs.virt-manager.enable = true;

  # DNSCrypt
  services.dnscrypt-proxy2 = {
    enable = true;
    settings = {
      ipv4_servers = true;
      require_dnssec = true;
      dnscrypt_servers = true;
      require_nolog = true;
      require_nofilter = true;
      #skip_incompatible = true;
      #forwarding_rules = "/home/chris0ax/Projects/Code/sys-config/laptop/dnscrypt/forwarding-rules.txt";
      sources.public-resolvers = {
        urls = [
          "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
          "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
        ];
        cache_file = "/var/lib/dnscrypt-proxy/public-resolvers.md";
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      };

      sources.relays = {
        urls = [ "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md" "https://download.dnscrypt.info/resolvers-list/v3/relays.md" ];
        cache_file = "/var/lib/dnscrypt-proxy/relays.md";
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
        refresh_delay = 72;
        prefix = "";
      };

      server_names = [ "scaleway-fr" "techsaviors.org-dnscrypt" "dnscry.pt-mumbai-ipv4" "dnscry.pt-chisinau-ipv4" "v.dnscrypt.uk-ipv4" "dct-nl" "fluffycat-fr-01" "meganerd" "dnscry.pt-istanbul-ipv4" "dnscrypt.pl" "dns.digitalsize.net" ];
      anonymized_dns.routes = [
        {
          server_name = "*";
          via = [ "dnscry.pt-anon-madrid-ipv4" "dnscry.pt-anon-london-ipv4" "anon-gombadi-chennai" "anon-dnscrypt.uk-ipv4" "anon-cs-pt" "anon-cs-hungary" "anon-cs-bulgaria" "anon-dnswarden-swiss" "anon-sth-se" ];
        }
      ];
    };
  };
  systemd.services.dnscrypt-proxy2.serviceConfig = {
    StateDirectory = "dnscrypt-proxy";
  };


  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.enable = true;
  # Syncthing ports for discovery (21027/UDP) and syncing (22000/TCP 22000/UDP) are allowed
  networking.firewall.allowedTCPPorts = [ 53317 22000 ];
  networking.firewall.allowedUDPPorts = [ 53317 22000 21027 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
