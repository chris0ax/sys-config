(defvar runemacs/default-font-size 200)
  (setq inhibit-startup-message t)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (set-fringe-mode 10)

  (menu-bar-mode -1)

  (setq visible-bell t)

  (column-number-mode)
;; Enable line numbers for some modes
  (dolist (mode '(text-mode-hook
	    prog-mode-hook
	    conf-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 1))))
  ;; Disable line numbers for some modes
  (dolist (mode '(org-mode-hook
	    term-mode-hook
	    shell-mode-hook
	    treemacs-mode-hook
	    eshell-mode-hook
	    pdf-view-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 0))))

  ;; (global-display-line-numbers-mode t)

(defun efs/set-font-faces()
  (set-face-attribute 'default nil :family "Iosevka NF" :height runemacs/default-font-size)
  (set-face-attribute 'fixed-pitch nil :family "Iosevka NF" :height 150)
  (set-face-attribute 'variable-pitch nil :family "FiraMono Nerd Font Propo" :height 150 :weight 'normal))

(if (daemonp)
    (add-hook 'after-make-frame-functions
	  (lambda (frame)
	    (setq doom-modeline-icon t)
	    (with-selected-frame frame
	      (efs/set-font-faces))))
  (efs/set-font-faces))

(setq gc-cons-threshold (* 50 1000 1000))

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections"
	       (format "%.2f seconds"
	       (float-time
		(time-subtract after-init-time before-init-time)))
	       gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)
 ; Increase the amount of data Emacs reads from Process
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; Bootstrap for straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

  (straight-use-package 'use-package)
  (setq straight-use-package-by-default t)

(defun efs/org-font-setup ()
  (set-face-attribute 'org-document-title nil :font "FiraMono Nerd Font Propo" :weight 'bold :height 1.5)

  (dolist (face '((org-level-1 . 1.75)
                  (org-level-2 . 1.5)
                  (org-level-3 . 1.25)
                  (org-level-4 . 1.1)
                  (org-level-5 . 1.0)
                  (org-level-6 . 1.0)
                  (org-level-7 . 1.0)
                  (org-level-8 . 1.0)))
  (set-face-attribute (car face) nil :font "FiraMono Nerd Font Propo" :weight 'regular :height (cdr face)))
  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (custom-theme-set-faces
   'user
   '(org-block ((t (:inherit fixed-pitch :foreground unspecified))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-checkbox ((t (:inherit fixed-pitch))))
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table ((t (:inherit fixed-pitch))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))))

(straight-use-package 'org)
(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1)
  )

(use-package org
  :hook
  (org-mode . efs/org-mode-setup)
  :bind
  ("C-c a" . org-agenda)
  :config
  ; Hide tags of entries of org agenda
  (setq org-agenda-hide-torg-auto-align-tags nil
        org-tags-column 0
        org-catch-invisible-edits 'show-and-error
        org-special-ctrl-a/e t
        org-insert-heading-respect-content tags-regexp ".*")
  (setq org-hide-emphasis-markers t)
  (setq org-ellipsis "...")
  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil)
  (setq org-pretty-entities t)
  (setq org-use-sub-superscripts "{}")
  (setq org-startup-indented t)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t))

(set-terminal-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)

(use-package undo-fu)

(use-package vundo)

(defun rune/evil-hook ()
  (dolist (mode '(custom-mode
                  eshell-mode
                  git-rebase-mode
                  erc-mode
                  circe-server-mode
                  circe-chat-mode
                  circe-query-mode
                  sauron-mode
                  term-mode))
    (add-to-list 'evil-emacs-state-modes mode)))

(use-package evil
  :demand t
  :init
  (setq evil-undo-system 'undo-fu)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  (setq evil-respect-visual-line-mode t)
  :hook (evil-mode . rune/evil-hook)
  :config (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside visual-line-mode buffer
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :straight t
  :config
  (evil-collection-init))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

;; Bindings with general.el
(use-package general
  :config
  (general-evil-setup t)
  ;; Creates a function for future key definitions
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")
  (general-create-definer dw/ctrl-c-keys
    :prefix "C-c")
  (rune/leader-keys
    "t" '(:ignore t :which-key "toggles")
    ;"tt" '(counsel-load-theme :which-key "choose theme")
    ))

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

  (rune/leader-keys
    "ts" '(hydra-text-scale/body :which-key "scale text"))

(use-package hl-line
  :custom
  (global-hl-line-mode +1))

(use-package pulsar
  :config
  (setq pulsar-pulse t)
  (setq pulsar-delay 0.055)
  (setq pulsar-iterations 10)
  (setq pulsar-face 'pulsar-magenta)
  (setq pulsar-highlight-face 'pulsar-yellow)
  :bind
  (("C-c h p" . pulsar-pulse-line)
   ("C-c h h" . pulsar-highlight-line))
  :custom
  (pulsar-global-mode +1)
  (pulsar-pulse-functions
   '(
     evil-window-up
     evil-window-down
     evil-window-left
     evil-window-right
     backward-page
     bookmark-jump
     delete-other-windows
     delete-window
     evil-goto-first-line
     evil-goto-line
     evil-scroll-down
     evil-scroll-line-to-bottom
     evil-scroll-line-to-center
     evil-scroll-line-to-top
     evil-scroll-page-down
     evil-scroll-page-up
     evil-scroll-up
     forward-page
     goto-line
     handle-switch-frame
     logos-backward-page-dwim
     logos-forward-page-dwim
     handle-select-window
     move-to-window-line-top-bottom
     narrow-to-defun
     narrow-to-page
     narrow-to-region
     next-buffer
     next-multiframe-window
     org-backward-heading-same-level
     org-forward-heading-same-level
     org-next-visible-heading
     org-previous-visible-heading
     other-window
     outline-backward-same-level
     outline-forward-same-level
     outline-next-visible-heading
     outline-previous-visible-heading
     outline-up-heading
     previous-buffer
     recenter-top-bottom
     reposition-window
     scroll-down-command
     scroll-up-command
     tab-close
     tab-new
     tab-next
     widen
     windmove-down
     windmove-left
     windmove-right
     windmove-swap-states-down
     windmove-swap-states-left
     windmove-swap-states-right
     windmove-swap-states-up
     windmove-up
     )))

(use-package indent-bars
  :straight (indent-bars :type git :host github :repo "jdtsmith/indent-bars")
  :custom
  ;; Add other languages as needed
  (indent-bars-treesit-scope '((python function_definition class_definition for_statement
                                       if_statement with_statement while_statement)))
  ;; wrap may not be needed if no-descend-list is enough
  ;;(indent-bars-treesit-wrap '((python argument_list parameters ; for python, as an example
  ;;				      list list_comprehension
  ;;				      dictionary dictionary_comprehension
  ;;				      parenthesized_expression subscript)))
  :hook ((python-base-mode yaml-mode) . indent-bars-mode)
  :config
  (setq
   indent-bars-color '(highlight :face-bg t :blend 0.3)
   indent-bars-pattern " . . . . ." ; play with the number of dots for your usual font size
   indent-bars-width-frac 0.25
   indent-bars-pad-frac 0.1)
  )

(use-package dashboard
  :straight t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-display-icons-p t)
  (setq dashboard-icon-type 'all-the-icons)
  (setq dashboard-projects-switch-function 'consult-projectile-switch-project)
  (setq dashboard-projects-backend 'projectile)
  (setq initial-buffer-choice (lambda () (get-buffer-create dashboard-buffer-name))))

(use-package doom-themes
   :straight t
   :config
   (load-theme 'doom-horizon t))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; First time this configuration is loaded on your machine
  ;; run the following command interactively:
  ;; M-x all-the-icons-install-fonts

(use-package all-the-icons
    :if (display-graphic-p))

(use-package minions
    :hook (doom-modeline-mode . minions-mode))

(use-package doom-modeline
  :straight t
  :hook (after-init . doom-modeline-mode)
  :custom-face
  (mode-line ((t (:height 0.85))))
  (mode-line-inactive ((t (:height 0.85))))
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-except-project)
  (doom-modeline-lsp t))

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

(use-package perspective
  :demand t
  :bind (("C-x b" . consult-buffer)
         ("C-M-k" . persp-switch)
         ("C-M-n" . persp-next)
         ("C-x k" . persp-kill-buffer*))
  :custom
  (persp-initial-frame-name "Main")
  (persp-mode-prefix-key (kbd "C-c M-p"))
  (persp-state-default-file "~/.emacs.d/persp-state")
  :config
  ;; Running `persp-mode' multiple times resets the perspective list...
  (unless (equal persp-mode t)
    (persp-mode)))

(customize-set-variable 'display-buffer-base-action
  '((display-buffer-reuse-window display-buffer-same-window)
    (reusable-frames . t)))

(customize-set-variable 'even-window-sizes nil)     ; avoid resizing

(use-package savehist
  :init
  (savehist-mode)
  :config
  (setq history-length 100))

(defun dw/minibuffer-backward-kill (arg)
   "When minibuffer is completing a file name delete up to parent
 folder, otherwise delete a word"
   (interactive "p")
   (if minibuffer-completing-file-name
       ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
       (if (string-match-p "/." (minibuffer-contents))
           (zap-up-to-char (- arg) ?/)
         (delete-minibuffer-contents))
       (delete-word (- arg))))

(use-package vertico
  :bind (:map vertico-map
	  ("C-j" . vertico-next)
	  ("C-k" . vertico-previous)
	  ("C-f" . vertico-exit)
	 :map minibuffer-local-map
	 ("M-h" . dw/minibuffer-backward-kill))
  :custom
  (vertico-cycle t)
  :custom-face
  (vertico-current ((t (:background "#3a3f5a"))))
  :init
  (vertico-mode))

(use-package yasnippet-capf
  :after cape
  :config
  (add-to-list 'completion-at-point-functions #'yasnippet-capf))

(defun chr/cape-capf-setup-elisp()
  "Replace the default `elisp-completion-at-point'
completion-at-point-function. Doing it this way will prevent
disrupting the addition of other capfs (e.g. merely setting the
variable entirely, or adding to list).

Additionally, add `cape-file' as early as possible to the list."
  (add-to-list 'completion-at-point-functions #'elisp-completion-at-point)
  (add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  ;; I prefer this being early/first in the list
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'yasnippet-capf))

(defun chr/cape-capf-setup-lsp ()
  "Replace the default `lsp-completion-at-point' with its
  `cape-capf-buster' version. Also add `cape-file' and
  `company-yasnippet' backends."
  (add-to-list 'completion-at-point-functions (cape-capf-buster #'lsp-completion-at-point))
  (add-to-list 'completion-at-point-functions #'yasnippet-capf)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev t)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-file))

(defun chr/cape-capf-setup-org ()
  (let (result)
  (dolist (element (list
	                  (cape-capf-super #'cape-dict #'cape-dabbrev))
	                 result)
    (add-to-list 'completion-at-point-functions element)))

(add-to-list 'completion-at-point-functions #'cape-elisp-block)
(add-to-list 'completion-at-point-functions #'yasnippet-capf)
(add-to-list 'completion-at-point-functions #'cape-keyword))

(defun chr/cape-capf-setup-eshell ()
  (add-to-list 'completion-at-point-functions #'cape-history)
  (add-to-list 'completion-at-point-functions #'cape-file))

(defun chr/cape-capf-setup-git-commit ()
  (let ((result))
    (dolist (element '(cape-dabbrev cape-keyword) result)
      (add-to-list 'completion-at-point-functions element))))

(use-package cape
  :hook
  (emacs-lisp-mode . chr/cape-capf-setup-elisp)
  (lsp-completion-mode . chr/cape-capf-setup-lsp)
  (org-mode . chr/cape-capf-setup-org)
  (eshell-mode . chr/cape-capf-setup-eshell)
  (git-commit-mode . chr/cape-capf-setup-git-commit)
  (LaTeX-mode . chr/cape-capf-setup-latex))

(defun corfu-enable-always-in-minibuffer ()
  "Enable Corfu in the minibuffer if Vertico/Mct are not active."
  (unless (or (bound-and-true-p mct--active)
              (bound-and-true-p vertico--input)
              (eq (current-local-map) read-passwd-map))
    ;; (setq-local corfu-auto nil) ;; Enable/disable auto completion
    (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                corfu-popupinfo-delay nil)
    (corfu-mode 1)))

(defun kb/corfu-setup-lsp ()
    "Use orderless completion style with lsp-capf instead of the
default lsp-passthrough."
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless)))

(use-package nerd-icons-corfu
  :after corfu)

(use-package corfu
  :bind
  (:map corfu-map
   ("TAB" . corfu-next)
   ([tab] . corfu-next)
   ("S-TAB" . corfu-previous)
   ([backtab] . corfu-previous))
  :init
  (corfu-history-mode)
  (corfu-popupinfo-mode)
  (global-corfu-mode)
  :custom
  (corfu-auto t)
  (corfu-auto-prefix 2)
  (corfu-quit-at-boundary 'separator)
  (corfu-preview-current 'insert)
  (corfu-preselect 'prompt)
  (corfu-cycle t)
  (corfu-separator ?\s)
  (tab-always-indent 'complete)
  :config
  (add-hook 'lsp-completion-mode-hook #'kb/corfu-setup-lsp)
  (with-eval-after-load 'safehist
    (cl-pushnew 'corfu-history savehist-additional-variables))
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter)
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(defun dw/get-project-root ()
  (when (fboundp 'projectile-project-root)
    (projectile-project-root)))

(use-package consult
  :demand t
  :bind (("C-s" . consult-line)
	 ("C-M-l" . consult-imenu)
	 ("C-M-j" . persp-switch-to-buffer*)
	 :map minibuffer-local-map
	 ("C-r" . consult-history))
  :config
  ;; local modes added to prog-mode hooks
  (add-to-list 'consult-preview-allowed-hooks 'elide-head-mode)
  ;; enabled global modes
  (add-to-list 'consult-preview-allowed-hooks 'global-org-modern-mode)
  :custom
  (consult-customize consult--source-buffer :hidden t :default nil)
  (add-to-list 'consult-buffer-sources persp-consult-source)
  (consult-project-root-function #'dw/get-project-root)
  (completion-in-region-function #'consult-completion-in-region))

(use-package wgrep)

(use-package consult-dir
  :after consult
  :straight t
  :bind (("C-x C-d" . consult-dir)
	 :map vertico-map
	 ("C-x C-d" . consult-dir)
	 ("C-x C-j" . consult-dir-jump-file))
  :custom
  (consult-dir-project-list-function nil))

(with-eval-after-load 'eshell-mode
(defun eshell/z (&optional regexp)
  "Navigate to a previously visited directory in eshell."
  (let ((eshell-dirs (delete-dups (mapcar 'abbreviate-file-name
                                          (ring-elements eshell-last-dir-ring)))))
    (cond
     ((and (not regexp) (featurep 'consult-dir))
      (let* ((consult-dir--source-eshell `(:name "Eshell"
                                                 :narrow ?e
                                                 :category file
                                                 :face consult-file
                                                 :items ,eshell-dirs))
             (consult-dir-sources (cons consult-dir--source-eshell consult-dir-sources)))
        (eshell/cd (substring-no-properties (consult-dir--pick "Switch directory: ")))))
     (t (eshell/cd (if regexp (eshell-find-previous-directory regexp)
                     (completing-read "cd: " eshell-dirs))))))))

(use-package marginalia
  :after vertico
  :bind
  (:map minibuffer-local-map
       ("M-A" . marginalia-cycle))
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package embark
  :bind (("C-S-a" . embark-act)
         ("C-;" . embark-dwim)
	       :map minibuffer-local-map
	       ("C-d" . embark-act))
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  (setq embark-action-indicator
        (lambda (map)
          (which-key--show-keymap "Embark" map nil nil 'no-paging)
          #'which-key--hide-popup-ignore-command)
        embark-become-indicator embark-action-indicator))

(use-package embark-consult
  :after embark
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :straight t
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key)
  )

(use-package avy
  :commands (avy-goto-char avy-goto-word-0 avy-goto-line))

(rune/leader-keys
  "j"   '(:ignore t :which-key "jump")
  "jj"  '(avy-goto-char :which-key "jump to char")
  "jw"  '(avy-goto-word-0 :which-key "jump to word")
  "jl"  '(avy-goto-line :which-key "jump to line"))

(use-package popper
  :straight t
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-group-function #'popper-group-by-perspective)
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          "^\\*Aweshell*" eshell-mode
          "^\\*vterm.*\\*$" vterm-mode
          "^\\*shell.*\\*$"  shell-mode 
          "^\\*term.*\\*$"   term-mode
          help-mode
          helpful-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

(use-package org-contrib)
(use-package auctex
  :config
  (require 'ox-latex)
  (require 'ox-extra)
  (ox-extras-activate '(latex-header-blocks ignore-headlines))
  (setq org-latex-pdf-process
        '("pdflatex -interaction nonstopmode -output-directory %o %f"
          "bibtex %b"
          "pdflatex -interaction nonstopmode -output-directory %o %f"
          "pdflatex -interaction nonstopmode -output-directory %o %f"))
  (setq org-latex-with-hyperref nil)
  (setq org-latex-logfiles-extensions
      (quote ("lof" "lot" "tex~" "aux" "idx" "log" "out" "toc" "nav" "snm" "vrb" "dvi" "fdb_latexmk" "blg" "brf" "fls" "entoc" "ps" "spl" "bbl" "xmpi" "run.xml" "bcf" "acn" "acr" "alg" "glg" "gls" "ist")))
)

(use-package evil-tex
  :hook
  (latex-mode . evil-tex-mode))

(use-package rainbow-mode
  :defer t
  :hook (org-mode
         emacs-lisp-mode
         web-mode
         typescript-mode
         js2-mode))

(use-package expand-region
  :bind (("M-[" . er/expand-region)
         ("C-(" . er/mark-outside-pairs)))

(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)

(setq-default indent-tabs-mode nil)

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(use-package magit
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package forge
  :after magit)

(use-package centaur-tabs
    :straight t
    :init
    (setq centaur-tabs-enable-key-bindings t)
    :config
    (centaur-tabs-mode t)
    (setq centaur-tabs-style "bar"
            centaur-tabs-height 32
            centaur-tabs-set-icons t
            centaur-tabs-gray-out-icons 'buffer
            centaur-tabs-show-new-tab-button t
            centaur-tabs-set-modified-marker t
            centaur-tabs-show-navigation-buttons t
            centaur-tabs-set-bar 'under
            centaur-tabs-show-count nil
            x-underline-at-descent-line t
            centaur-tabs-adjust-buffer-order t
            centaur-tabs-left-edge-margin nil)
    (centaur-tabs-change-fonts (face-attribute 'default :font) 110)
    (centaur-tabs-headline-match)
    (defun centaur-tabs-buffer-groups ()
    "`centaur-tabs-buffer-groups' control buffers' group rules.

Group centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode' `dired-mode' `org-mode' `magit-mode'.
All buffer name start with * will group to \"Emacs\".
Other buffer group by `centaur-tabs-get-group-name' with project name."
    (list
     (cond
      ;; ((not (eq (file-remote-p (buffer-file-name)) nil))
      ;; "Remote")
      ((or (string-equal "*" (substring (buffer-name) 0 1))
           (memq major-mode '(magit-process-mode
                              magit-status-mode
                              magit-diff-mode
                              magit-log-mode
                              magit-file-mode
                              magit-blob-mode
                              magit-blame-mode
                              )))
       "Emacs")
      ((derived-mode-p 'prog-mode)
       "Editing")
      ((derived-mode-p 'dired-mode)
       "Dired")
      ((memq major-mode '(helpful-mode
                          help-mode))
       "Help")
      ((memq major-mode '(org-mode
                          org-agenda-clockreport-mode
                          org-src-mode
                          org-agenda-mode
                          org-beamer-mode
                          org-indent-mode
                          org-bullets-mode
                          org-cdlatex-mode
                          org-agenda-log-mode
                          diary-mode))
       "OrgMode")
      (t
       (centaur-tabs-get-group-name (current-buffer))))))
    :hook
    (dashboard-mode . centaur-tabs-local-mode)
    (term-mode . centaur-tabs-local-mode)
    (eshell-mode . centaur-tabs-local-mode)
    ;(eat-mode . centaur-tabs-local-mode)
    (calendar-mode . centaur-tabs-local-mode)
    (org-agenda-mode . centaur-tabs-local-mode)
    :bind
    ("C-<prior>" . centaur-tabs-backward)
    ("C-<next>" . centaur-tabs-forward)
    ("C-S-<prior>" . centaur-tabs-move-current-tab-to-left)
    ("C-S-<next>" . centaur-tabs-move-current-tab-to-right)
    (:map evil-normal-state-map
  	  ("g t" . centaur-tabs-forward)
  	  ("g T" . centaur-tabs-backward)))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-sideline-show-code-actions t)
  (setq lsp-ui-doc-position 'bottom))

(defun efs/lsp-mode-setup()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :hook
  (python-mode . lsp-mode)
  (go-mode . lsp-mode)
  (lsp-mode . efs/lsp-mode-setup)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :bind
  ("M-RET" . lsp-execute-code-action)
  :custom
  (lsp-warn-no-matched-clients nil)
  :config
  (setq lsp-enable-which-key-integration t)
  (setq python-shell-interpreter "python")
  (setq python-indent-offset 4)
  (setq python-check-command "ruff")
  (setq lsp-ruff-lsp-python-path "python"))

(use-package lsp-treemacs
  :after lsp
  :config
  (lsp-treemacs-sync-mode 1))

(use-package flycheck
  :straight t
  :after lsp
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  )

(use-package yasnippet
  :init
  (yas-global-mode 1))
(use-package yasnippet-snippets
  :after yasnippet)

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp))))  ; or lsp-deferred

(use-package lsp-latex
  :after tex-mode
  :hook
  (tex-mode . lsp)
  (latex-mode . lsp)
  (bibtex-mode . lsp))

(use-package markdown-mode
  :straight t
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "multimarkdown")
  :bind (:map markdown-mode-map
         ("C-c C-e" . markdown-do)))

(use-package dap-mode
  :after lsp-mode
  :hook
  ((python-mode . dap-ui-mode)
   (python-mode . dap-mode)
   (go-mode . dap-ui-mode)
   (go-mode . dap-mode))
  :config
  (setq dap-ui-mode 1)
  (setq dap-tooltip-mode 1)
  (setq dap-ui-controls-mode 1)
  (require 'dap-python)
  (setq dap-python-debugger 'debugpy)
  (dap-auto-configure-mode)
  (add-hook 'dap-stopped-hook
            (lambda (arg) (call-interactively #'dap-hydra))))

(use-package go-mode
  :hook
  (go-mode . lsp-deferred)
  :config
  (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode)))

(use-package gorepl-mode
  :after (go-mode)
  :hook
  (go-mode . gorepl-mode))

(use-package gotest
  :after go-mode)

(defun my/go-playground-remove-lsp-workspace () (when-let ((root (lsp-workspace-root))) (lsp-workspace-folders-remove root)))

(use-package go-playground
  :after go-mode
  :config
  (add-hook 'go-playground-pre-rm-hook #'my/go-playground-remove-lsp-workspace))

(use-package yaml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))

(use-package yaml-pro
  :config
  (add-hook 'yaml-mode-hook 'yaml-pro-ts-mode 100))

(use-package plantuml-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
  :config
 ; (setq plantuml-executable-path "/run/current-system/sw/bin/plantuml")
  (setq plantuml-default-exec-mode 'executable))

(use-package json-mode)

(use-package powershell
  :config
  (add-to-list 'auto-mode-alist '("\\.ps1\\'" . powershell-mode))
  )

(use-package ob-powershell)

;  (use-package koopa-mode
 ;   :config
  ;  (add-to-list 'auto-mode-alist '("\\.ps1\\'" . koopa-mode))
  ;  (add-to-list 'koopa-powershell-cli-arguments "-NoLogo")
  ;  (setq koopa-powershell-executable "powershell")
  ;  (setq koopa-is-running-on-windows t)
  ;  )

(use-package treesit-auto
  :custom
  (treesit-auto-langs '(python go bash))
  (treesit-auto-add-to-auto-mode-alist 'all)
  (treesit-auto-install t)
  :config
  (global-treesit-auto-mode))

(use-package docker
  :ensure t
  :bind ("C-c d" . docker))

(use-package terraform-mode
  :straight t

  :custom (terraform-indent-level 4)
  :config
  (defun my-terraform-mode-init ()
    (outline-minor-mode 1)
    )
  (add-hook 'terraform-mode-hook 'my-terraform-mode-init))

(use-package ansible)

(defun dw/switch-project-action ()
  "Switch to a workspace with the project name."
  (persp-switch (projectile-project-name)))

  (use-package projectile
    :diminish projectile-mode
    :config (projectile-mode)
    :demand t
    :bind ("C-M-p" . projectile-find-file)
    :bind-keymap
    ("C-c p" . projectile-command-map)
    :init
    (when (file-directory-p "~/Projects/Code")
      (setq projectile-project-search-path '("~/Projects/Code"))

    (setq projectile-switch-project-action #'dw/switch-project-action)))

(use-package consult-projectile
  :after projectile)
(rune/leader-keys
  "pF" 'consult-ripgrep)

(use-package org-appear
  :after org
  :hook
  (org-mode . org-appear-mode))

(use-package org-fragtog
    :after org
    :custom
    (org-startup-with-latex-preview t)
    :hook
    (org-mode . org-fragtog-mode)
    :custom
    (org-format-latex-options
     (plist-put org-format-latex-options :scale 2)
     (plist-put org-format-latex-options :foreground 'auto)
     (plist-put org-format-latex-options :background 'auto)))

(use-package org-modern
  :straight t
  :custom
  (org-modern-list 
   '((?- . "-")
     (?* . "•")
     (?+ . "‣")))
  (org-modern-block-name '("" . ""))
  :custom
  (org-modern-table nil)
  :config
  (add-hook 'org-agenda-finalize-hook #'org-modern-agenda)
  :hook
  (org-mode . org-modern-mode))
  

(use-package org-modern-indent
  :demand t
  :straight (org-modern-indent :type git :host github :repo "jdtsmith/org-modern-indent")
  :config
  (add-hook 'org-mode-hook #'org-modern-indent-mode 90))

(use-package ob-mermaid
  :after org)

(with-eval-after-load 'org
  (setq org-babel-lisp-eval-fn 'sly-eval)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (java . t)
     (mermaid . t)
     (plantuml . t)
     (powershell . t)))
  (push '("conf-unix" . conf-unix) org-src-lang-modes)
  (push '("plantuml" . plantuml) org-src-lang-modes))

(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

(with-eval-after-load 'org
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python")))
(add-to-list 'org-structure-template-alist '("li" . "src lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :defer t
  :hook (org-mode . efs/org-mode-visual-fill)
  :config
  (add-hook 'org-agenda-finalize-hook #'visual-fill-column-mode))

(use-package org-download
  :straight (org-download :type git :host github :repo "abo-abo/org-download")
  :config
  (add-hook 'dired-mode-hook 'org-download-enable)
  (rune/leader-keys
    "pp"  '(org-download-clipboard :which-key "Paste Image from Clipboard")))

(use-package org-cliplink
  :after org
  :bind
  ("C-x p i" . org-cliplink))

(defun org-image-link (protocol link _description)
  "Interpret LINK as base64-encoded image data."
  (cl-assert (string-match "\\`img" protocol) nil
             "Expected protocol type starting with img")
  (let ((buf (url-retrieve-synchronously (concat (substring protocol 3) ":" link))))
    (cl-assert buf nil
               "Download of image \"%s\" failed." link)
    (with-current-buffer buf
      (goto-char (point-min))
      (re-search-forward "\r?\n\r?\n")
      (buffer-substring-no-properties (point) (point-max)))))

(use-package org-yt
    :straight (org-yt :type git :host github :repo "TobiasZawada/org-yt")
    :config
    (org-link-set-parameters
     "imghttp"
     :image-data-fun #'org-image-link)
    
    (org-link-set-parameters
     "imghttps"
     :image-data-fun #'org-image-link))

(use-package org-transclusion
  :after org
  :config
  (define-key global-map (kbd "<f12>") #'org-transclusion-add)
  (rune/leader-keys
      "nt" '(org-transclusion-mode :which-key "Turn on Org-Transclusion")))

(use-package toc-org
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

(with-eval-after-load 'markdown-mode
  (define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point))

(use-package org-roam
  :straight t
  :after org
  :custom
  (org-roam-directory (file-truename "~/Org-Roam-Notes/"))
  (org-roam-dailies-directory "Journal/")
  (setq org-roam-complete-everywhere t)
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  (org-roam-capture-templates
    '(("d" "default" plain
       "%?"
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
       :unnarrowed t)
      ("p" "project file" plain
       "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add Initial tasks \n\n * Dates \n\n"
       ; Add a tag of project to make it easy to query and filter org-roam notes
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;;Dailies
         ("C-c n j" . org-roam-dailies-capture-today)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  (require 'org-roam-dailies)
  (require 'org-roam-protocol))

(use-package org-roam-ui
  :straight
    (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
    :after org-roam
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(setq browse-url-firefox-program "firefox")
(setq browse-url-browser-function 'browse-url-firefox)

(use-package org-web-tools
  :bind
  ("C-c w w" . org-web-tools-insert-link-for-url))

(use-package dired
  :ensure nil
  :straight nil
  :commands (dired dired-jump)
  :bind ("C-x C-j" . dired-jump)
  :custom ((dired-listing-switches "-aho --group-directories-first"))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-up-directory
    "l" 'dired-find-file))

(use-package exec-path-from-shell)

(dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "NIX_PROFILES"))
    (add-to-list 'exec-path-from-shell-variables var))

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
(when (daemonp)
  (exec-path-from-shell-initialize))

(defun efs/configure-eshell ()
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
  (setq eshell-history-size 10000
    eshell-buffer-maximum-lines 10000
    eshell-hist-ignoredups t
    eshell-scroll-to-bottom-on-input t))

(defun corfu-send-shell (&rest _)
  "Send completion candidate when inside comint/eshell."
  (cond
  ((and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
    (eshell-send-input))
  ((and (derived-mode-p 'comint-mode)  (fboundp 'comint-send-input))
    (comint-send-input))))

(use-package eshell
  :hook
  (eshell-first-time-mode . efs/configure-eshell)
  :config
  (advice-add #'corfu-insert :after #'corfu-send-shell)
 
  (add-hook 'eshell-mode-hook
            (lambda ()
              (setq-local
               corfu-quit-at-boundary t
               corfu-quit-no-match t
               corfu-auto nil)
              (corfu-mode)))
  (setq eshell-aliases-file "~/.emacs.d/.eshell.aliases")
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim" "gpg"))))

(use-package eshell-up
  :after eshell)

(use-package eshell-prompt-extras
  :after eshell)

(with-eval-after-load 'esh-opt
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-highlight-prompt nil
        eshell-prompt-function 'epe-theme-lambda))

(use-package aweshell
  :straight (aweshell
             :type git
             :host github
             :repo "manateelazycat/aweshell")
  :after eshell
  :bind
  ("C-c e t" . aweshell-new)
  ("C-c e n" . aweshell-next)
  ("C-c e p" . aweshell-prev)
  ("C-c e d" . aweshell-dedicated-toggle))

(setenv  "PATH" (concat
                 (concat (getenv "HOME") "\\AppData\\Roaming\\Python\\Python312\\Scripts")
                 ;; Unix tools 
                 "C:\\Program Files\\Git\\usr\\bin" ";"
                 (getenv "PATH")
         ))

;(add-to-list 'exec-path "C:\\Program Files\\Git\\usr\\bin")
;(setq exec-path (append '("C:\\Program Files\\Git\\usr\\bin")
 ;                     exec-path))

(defun run-bash ()
      (interactive)
      (let ((shell-file-name "C:\\Program Files\\Git\\bin\\bash.exe"))
            (shell "*bash*")))

(defun run-cmdexe ()
    (interactive)
    (let ((shell-file-name "cmd.exe"))
      (shell "*cmd.exe*")))

(setq gc-cons-threshold (* 2 1000 1000))
